<?php

namespace Frontend\Modules\Brokers\Actions;

use Frontend\Core\Engine\Base\Block as FrontendBaseBlock;
use Frontend\Core\Engine\Model as FrontendModel;
use Frontend\Modules\Brokers\Engine\Model as FrontendBrokersModel;
use Frontend\Modules\Objects\Engine\Model as FrontendObjectsModel;
use Frontend\Core\Engine\Form as FrontendForm;
use Frontend\Core\Engine\Language as FL;

class GoogleRecaptcha
{
    /* Google recaptcha API url */
    private $google_url = "https://www.google.com/recaptcha/api/siteverify";
    private $secret = '6LdpqAUTAAAAAKPD4dTXbumd40a1brrizdyxJ7ED';

    public function VerifyCaptcha($response)
    {
        $url = $this->google_url."?secret=".$this->secret.
            "&response=".$response;

        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($curl, CURLOPT_TIMEOUT, 15);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, FALSE);
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, FALSE);
        $curlData = curl_exec($curl);

        curl_close($curl);

        $res = json_decode($curlData, TRUE);
        if($res['success'] == 'true')
            return TRUE;
        else
            return FALSE;
    }

}

class Index extends FrontendBaseBlock
{
    /**
     * @var    array
     */
    private $items = array();
    private $broker = array();
    private $objects = array();
    private $feedbacks = array();
    private $frm = array();
    /**
     * Execute the extra
     */
    public function execute()
    {
        parent::execute();
        $this->loadForm();
        $this->getData();
        $this->loadTemplate();
        $this->parse();
    }

    private function getData() {
        $id = \SpoonFilter::getGetValue('id', null, '');
        if ((int)$id) {
            $this->insertFeedBack($id);
            $this->broker = FrontendBrokersModel::getBroker($id);
            $photo = FrontendBrokersModel::getBrokerPhoto($id);
            $feedbacks = FrontendBrokersModel::getBrokerFeedBacks($id);
            if($feedbacks) {
                $this->feedbacks = FrontendBrokersModel::getBrokerFeedBacks($id);
            }
            $this->broker['photo'] = $photo['photo'];
            $this->objects = FrontendBrokersModel::getObjectsByBroker($id);
            foreach ($this->objects as $key => $item) {
                $photo = FrontendObjectsModel::getMainPhoto($item['id']);
                $this->objects[$key]['photo'] = $photo['photo'];
            }
        } else {
            $this->items = FrontendBrokersModel::getBrokers();
            foreach ($this->items as $key => $item) {
                $photo = FrontendBrokersModel::getBrokerPhoto($item['id']);
                $this->items[$key]['photo'] = $photo['photo'];

                $statusName = '';
                if ($item['level'] == 1) {
                    $statusName = '';
                } elseif($item['level'] == 4) {
                    $statusName = '<b>TOP 1</b>';
                } elseif($item['level'] == 3){
                    $statusName = '<b>TOP 2</b>';
                } elseif($item['level'] == 2) {
                    $statusName = '<b>TOP 3</b>';
                }

                $this->items[$key]['statusName'] = $statusName;
            }
        }
    }

    private function loadForm()
    {
        $this->frm = new FrontendForm('feedback');

        $this->frm->addText('name', null, null, 'form-control', 'inputTextError title');
        $this->frm->addText('email', null, null, 'form-control', 'inputTextError title');
        $this->frm->addText('feedback', null, null, 'form-control', 'inputTextError title');
        $this->frm->addText('captcha', null, null, 'form-control', 'inputTextError title');
    }

    private function insertFeedBack($id)
    {
        if($_SERVER["REQUEST_METHOD"] == "POST")
        {
            $response = $_POST['g-recaptcha-response'];

            if(!empty($response))
            {
                $cap = new GoogleRecaptcha();
                $verified = $cap->VerifyCaptcha($response);

                if($verified) {
                    \SpoonSession::set('captcha', true);
                }
            }
        }

        if ($this->frm->isSubmitted()) {
            $this->frm->cleanupFields();
            $this->frm->getField('name')->isFilled(FL::err('NameIsRequired'));
            $this->frm->getField('feedback')->isFilled(FL::err('FeedBackIsRequired'));

            if (!\SpoonSession::exists('captcha') || !\SpoonSession::get('captcha')) {
                $this->tpl->assign('captchaError', 'true');
                return false;
            }

            if ($this->frm->isCorrect()) {
                \SpoonSession::set('captcha', false);

                $item['name'] = $this->frm->getField('name')->getValue();
                $item['email'] = $this->frm->getField('email')->getValue();
                $item['feedBack'] = $this->frm->getField('feedback')->getValue();
                $item['broker'] = $id;
                $item['date'] = gmdate('Y-m-d h:i:s', time());

                FrontendBrokersModel::insertFeedBack($item);

                $this->tpl->assign('feedBackAdded', 'yes');
            }
        }
    }

    private function parse()
    {
        $this->tpl->assign('brokers', $this->items);
        $this->tpl->assign('broker', $this->broker);
        $this->tpl->assign('objects', $this->objects);
        $this->tpl->assign('feedbacks', $this->feedbacks);
        $this->frm->parse($this->tpl);
    }
}
