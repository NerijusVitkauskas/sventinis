<section class="mod">
    <div class="inner">
        <div class="bd content">
            {option:brokers}
                <div class="row">
                    <p style="display:none;">
                        &nbsp; &nbsp; &nbsp; Mūsų biurai įsikūrę Vilniuje&nbsp;ir&nbsp;Kaune, kuriuose dirba virš 100 darbuotojų. Mūsų amžiaus vidurkis 34 metai, mūsų jauniausiam darbuotojui 22, o vyriausiam virš penkiasdešimt.<br>
                        &nbsp; &nbsp; &nbsp;Mūsų komandą&nbsp; sudaro 70 % vyrų ir tik 30 % moterų, kurios daugiausiai analizuoja, testuoja, dokumentuoja ir veda mokymus, vadovauja skyriams ir rūpinasi, kad visiems būtų gera ir patogu dirbti. Džiaugiamės, kad mūsų vaikinai lepina mus, o kartais ir kaimynes.</p>

                    <div class="brokersHeader">Mūsų komanda</div>
                    {iteration:brokers}
                        <div class="col-lg-3 col-md-4 col-xs-6" style="">
                            <div id="{$brokers.id}" class="brokerPhoto">
                                {option:brokers.statusName}
                                <div class="brokersNameStatusSmall"> {$brokers.statusName}</div>
                                {/option:brokers.statusName}

                                <img src="{$THEME_URL}/../../Files/brokers/220x290/{$brokers.photo}"/>
                                <div class="brokersName">
                                    <div class="brokersNameStatus">
                                        {$brokers.statusName}
                                    </div>
                                    <img class="dotsImage" src="{$THEME_URL}/Core/Layout/images/dots.png">
                                </div>
                                <div class="brokersInfo">
                                    <div class="brokersInfoInner">
                                        {$brokers.phone}<br>
                                        {$brokers.email}<br>
                                    </div>
                                </div>
                                <div class="brokersNameText">{$brokers.name}</div>
                            </div>
                        </div>
                    {/iteration:brokers}
                </div>
            {/option:brokers}


            {option:broker}
                <div class="row brokerInnerBlock">
                        <div class="col-lg-3 col-md-4 paddingZero">
                            <div class="brokerInnerImage" style = 'background-image: url({$THEME_URL}/../../Files/brokers/220x290/{$broker.photo})'></div>
                            <div class="brokerInnerContact"><img class="cp" src="{$THEME_URL}/Core/Layout/images/feedback.png"></div>
                        </div>

                        <div class="col-lg-9 col-md-8">
                            <div class="row">
                                <div class="col-lg-5 col-md-12">
                                    <div class="brokerInnerName">{$broker.name}</div>
                                </div>
                                <div class="col-lg-3 col-md-6 paddingZero">
                                    <div class="clearfix mt10">
                                        <div class="phoneImg dib vam"></div>
                                        <div class="brokerInnerPhone dib vam ml10">{$broker.phone}</div>
                                    </div>
                                </div>

                                <div class="col-lg-4 col-md-6 paddingZero">
                                    <div class="clearfix mt10">
                                        <div class="emailImg dib vam"></div>
                                        <a class="dib vam ml10" href="mailto:{$broker.email}">{$broker.email}</a>
                                    </div>
                                </div>
                                <div class="col-md-12" style="padding-left: 4px;">
                                    <div class="brokerInnerDescription">{$broker.description}</div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="brokerInnerItems row">
                        {option:objects}
                                {iteration:objects}
                                    <div id="{$objects.id}" class="col-md-4 col-xs-6 mrgb15 brokerInnerItem">
                                        <div id="{$objects.id}" class="objectsListItem">
                                            <div class="objectsListItemHover"><img src="{$THEME_URL}/Core/Layout/images/dots.png"></div>
                                            <img class="objectsListImage" src="/library/external/image.php?width=248&height=159&image={$objects.photo}"/>
                                            <div class="col-sm-7 street">{$objects.street}, {$objects.city}</div>
                                            <div class="col-sm-5 price"><span class="price pricePlus">{$objects.price} &#8364;</span></div>
                                        </div>
                                    </div>
                                {/iteration:objects}
                        {/option:objects}
                    </div>

            <div class="col-md-12 brokerInnerFeedback">
                <div class="feedBackHeader">
                    {option:feedbacks}Atsiliepimai{/option:feedbacks}
                    {option:!feedbacks}Atsiliepimų nėra{/option:!feedbacks}
                </div>
                <div class="feedbackButtonPlace">
                    <a class="btn btn-primary btn-sm feedbackButton" href="#divForm" id="btnForm">Rašyti atsiliepimą</a>
                </div>
                <div class="feedsBack">
                    {option:feedbacks}
                    {iteration:feedbacks}
                        <blockquote>
                             <div class="feedBackName">{$feedbacks.name}</div><div class="feedBackDate">{$feedbacks.date}</div>
                            <p>{$feedbacks.feedback}</p>
                        </blockquote>
                    {/iteration:feedbacks}
                    {/option:feedbacks}
                </div>
            </div>

            <div id="divForm" style="display:none">
                <div class="modal-header">
                    <h4 class="modal-title" id="myModalLabel">Rašyti atsiliepimą</h4>
                </div>
                <div class="modal-body" style="min-height: 180px;">
                    <script src='https://www.google.com/recaptcha/api.js'></script>
                    {form:feedback}

                    <div class="form-group">
                        <label class="col-sm-3 control-label" for="name">{$lblName|ucfirst}</label>

                        <div class="col-sm-9" style="height: 45px;">
                            {$txtName}
                        </div>
                    </div>

                    <div class="form-group mt15">
                        <label class="col-sm-3 control-label" for="name">{$lblEmail|ucfirst}</label>

                        <div class="col-sm-9" style="height: 45px;">
                            {$txtEmail}
                        </div>
                    </div>
                    <div class="form-group mb15">
                        <label class="col-sm-3 control-label" for="name">{$lblFeedBack|ucfirst}</label>

                        <div class="col-sm-9" style="height: 45px;">
                            <textarea id="feedback" name="feedback" type="text" class="form-control"></textarea>
                        </div>
                    </div>

                    <div class="form-group mb15">
                        <label style="margin-top: 20px;" class="col-sm-3 control-label" for="name"></la`bel>
                        <div class="col-sm-9" style="height: 45px; margin-top: 20px;">
                            <div style="float: right;" class="g-recaptcha" data-sitekey="6LdpqAUTAAAAAHgnO1XUPcHVYENDwbKT8Eg57opc"></div>
                            <div style="float: right">{option:captchaError}Klaida! Įrodykite, jog esate žmogus.{/option:captchaError}</div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" style="margin-top: 60px;background-color: #d76830;border-color: #BA5C2D;margin-right: 15px;"
                            class="btn btn-primary">Rašyti
                    </button>
                </div>
                {/form:feedback}
            </div>

        </div>
            {/option:broker}

        </div>
    </div>
    </div>
</section>