<?php

namespace Frontend\Modules\Brokers\Engine;

use Frontend\Core\Engine\Model as FrontendModel;
use Frontend\Core\Engine\Navigation as FrontendNavigation;
use Frontend\Core\Engine\Url as FrontendURL;

class Model
{
    public static function getBrokers() {
        $db = FrontendModel::getContainer()->get('database');

        return (array) $db->getRecords(
            'SELECT r.*
             FROM brokers AS r
             ORDER BY level DESC');
    }

    public static function getBroker($id) {
        $db = FrontendModel::getContainer()->get('database');

        return $db->getRecord(
            'SELECT r.*
             FROM brokers AS r
             WHERE r.id = ?
             ORDER BY id DESC', array((int)$id));
    }

    public static function getBrokerPhoto($id) {
        $db = FrontendModel::getContainer()->get('database');

        return $db->getRecord(
            'SELECT r.*
             FROM brokers_photos AS r
             WHERE r.broker = ?
             ORDER BY id DESC', array((int)$id));
    }

    public static function getBrokerFeedBacks($id) {
        $db = FrontendModel::getContainer()->get('database');

        return $db->getRecords(
            'SELECT r.*
             FROM brokers_feedbacks AS r
             WHERE r.broker = ?
             ORDER BY id DESC', array((int)$id));
    }

    public static function insertFeedBack(array $item)
    {
        $insertId = FrontendModel::getContainer()->get('database')->insert('brokers_feedbacks', $item);
        return $insertId;
    }


    public static function getObjectsByBroker($id) {
        $db = FrontendModel::getContainer()->get('database');

        return (array) $db->getRecords(
            'SELECT r.*
             FROM objects AS r
             WHERE r.broker = ?
             ORDER BY id DESC', array((int)$id));
    }
}
