<?php

namespace Frontend\Modules\Objects\Engine;

use Frontend\Core\Engine\Model as FrontendModel;
use Frontend\Core\Engine\Navigation as FrontendNavigation;
use Frontend\Core\Engine\Url as FrontendURL;
use Frontend\Modules\Objects\Engine\Model as FrontendObjectsModel;
use Frontend\Core\Engine\Language as FL;



class Helper
{
    public static function objectTypes($item)
    {
        $changeThis = array();
        $changeThis['apartment'] = "Butas";
        $changeThis['house'] = "Namas";
        $changeThis['land'] = "Sklypas";
        $changeThis['premises'] = "Patalpos";

        $changeThis['sale'] = " pardavimui";
        $changeThis['rent'] = " nuomai";

        if(isset($changeThis[$item])) {
            return $changeThis[$item];
        }

        return $item;
    }

    public static function changeValue($item)
    {
        foreach($item as $key => $value) {
            $translation = FL::lbl('Feature' . $value);
            $firstSym = substr($translation, 0, 1);
            if($firstSym != '{') {
                $newValues[$key] = $translation;
            } else {
                $newValues[$key] = $value;
            }
        }

        return $newValues;
    }

    public static function extraFeatures($item)
    {
        $translation = FL::lbl('Feature' . $item);
        $firstSym = substr($translation, 0, 1);

        if($firstSym != '{') {
            return $translation;
        } else {
            return $item;
        }
    }

    public static function getCities()
    {
        $cities = FrontendObjectsModel::getCities();
        return $cities;
    }

    public static function getRegions($city, $type)
    {
        $typeFind1 = $typeFind2 = NULL;
        if($type) {
            if ($type == 1) {
                $typeFind1 = 'apartment';
                $typeFind2 = 'sale';
            }
            if ($type == 2) {
                $typeFind1 = 'house';
                $typeFind2 = 'sale';
            }
            if ($type == 3) {
                $typeFind1 = 'land';
                $typeFind2 = 'sale';
            }
            if ($type == 4) {
                $typeFind1 = 'apartment';
                $typeFind2 = 'rent';
            }
            if ($type == 5) {
                $typeFind1 = 'house';
                $typeFind2 = 'rent';
            }
        }

        $regions = FrontendObjectsModel::getRegions($city, $typeFind1, $typeFind2);
        return $regions;
    }

    public static function getNumbers($to = 20)
    {
        $x = 0;
        $numbers = array();

        while($x < $to) {
            $x++;
            $numbers[]['value'] = $x;
        }
        return $numbers;
    }

}
