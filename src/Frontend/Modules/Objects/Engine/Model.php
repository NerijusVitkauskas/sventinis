<?php

namespace Frontend\Modules\Objects\Engine;

use Frontend\Core\Engine\Model as FrontendModel;
use Frontend\Core\Engine\Navigation as FrontendNavigation;
use Frontend\Core\Engine\Url as FrontendURL;

class Model
{
    public static function getAllObjectsCrmIds() {
        $db = FrontendModel::getContainer()->get('database');

        return (array) $db->getRecords(
            'SELECT r.crmId, r.id
             FROM objects AS r
             ORDER BY id DESC');
    }

    public static function deleteObject($id)
    {
        $db = FrontendModel::getContainer()->get('database');
        $db->delete('objects', 'crmId = '. (int)$id);
    }

    public static function deletePhoto($objectId)
    {
        $db = FrontendModel::getContainer()->get('database');
        $db->delete('objects_photos', 'object = '. (int)$objectId);
    }

    public static function deletePhotoByCrmId($id)
    {
        $db = FrontendModel::getContainer()->get('database');
        $db->delete('objects_photos', 'crm_id = '. (int)$id);
    }

    public static function getObjects() {
        $db = FrontendModel::getContainer()->get('database');

        return (array) $db->getRecords(
            'SELECT r.*
             FROM objects AS r
             ORDER BY id DESC limit 15');
    }

    public static function getObjectsByFilter($lastId, $type, $street, $typeFind1, $typeFind2, $city, $region, $areaFrom, $areaTo, $groundAreaFrom, $groundAreaTo, $priceFrom, $priceTo, $roomsFrom, $roomsTo, $floorFrom, $floorTo, $purpose) {
        $db = FrontendModel::getContainer()->get('database');
        $values = array();

        if(!$lastId) {
            $sql = 'WHERE r.expiredDate > ?';
            $values[] = 1;

            if ($typeFind1) {
                $sql = $sql . ' AND r.objectType = ?';
                $sql = $sql . ' AND r.dealType = ?';
                $values[] = $typeFind1;
                $values[] = $typeFind2;
            }

            if ($city) {
                $sql = $sql . ' AND r.city = ?';
                $values[] = $city;
            }

            if ($region) {
                $sql = $sql . ' AND r.region = ?';
                $values[] = $region;
            }

            if ($street) {
                $sql = $sql . " AND r.street REGEXP ?";
                $values[] = $street;
            }

            if ($priceFrom) {
                $sql = $sql . ' AND r.price >= ?';
                $values[] = $priceFrom;
            }

            if ($priceTo) {
                $sql = $sql . ' AND r.price <= ?';
                $values[] = $priceTo;
            }

            if ($type == 2 || $type == 5) {
                if ($groundAreaFrom) {
                    $sql = $sql . ' AND r.space >= ?';
                    $values[] = $groundAreaFrom;
                }

                if ($groundAreaTo) {
                    $sql = $sql . ' AND r.space <= ?';
                    $values[] = $groundAreaTo;
                }

                if ($areaFrom) {
                    $sql = $sql . ' AND r.groundSpace >= ?';
                    $values[] = $areaFrom;
                }

                if ($areaTo) {
                    $sql = $sql . ' AND r.groundSpace <= ?';
                    $values[] = $areaTo;
                }
            } else {
                if ($areaFrom) {
                    $sql = $sql . ' AND r.space >= ?';
                    $values[] = $areaFrom;
                }

                if ($areaTo) {
                    $sql = $sql . ' AND r.space <= ?';
                    $values[] = $areaTo;
                }
            }

            if ($roomsFrom) {
                $sql = $sql . ' AND r.rooms >= ?';
                $values[] = $roomsFrom;
            }

            if ($roomsTo) {
                $sql = $sql . ' AND r.rooms <= ?';
                $values[] = $roomsTo;
            }

            if ($floorFrom) {
                $sql = $sql . ' AND r.floor >= ?';
                $values[] = $floorFrom;
            }

            if ($floorTo) {
                $sql = $sql . ' AND r.floor <= ?';
                $values[] = $floorTo;
            }

            if ($purpose) {
                $sql = $sql . ' AND r.land_purpose <= ?';
                $values[] = $purpose;
            }

            \SpoonSession::set('filterSql', $sql);
            \SpoonSession::set('filterValues', serialize($values));
        } else {
            $sql = \SpoonSession::get('filterSql');
            $values = unserialize(\SpoonSession::get('filterValues'));

            $sql = $sql . ' AND r.id < ?';
            $values[] = $lastId;
        }


        return (array) $db->getRecords(
            'SELECT r.*
             FROM objects AS r
             ' .$sql. '
              ORDER BY id DESC limit 15', $values);
    }

    public static function getCities() {
        $db = FrontendModel::getContainer()->get('database');

        return $db->getRecords(
            'SELECT r.city, r.id
             FROM objects AS r
             GROUP BY r.city', array());
    }

    public static function getRegions($city, $typeFind1, $typeFind2) {
        $db = FrontendModel::getContainer()->get('database');

        $sql = 'WHERE r.city = ?';
        $values[] = $city;

        if($typeFind1) {
            $sql = $sql . ' AND r.objectType = ?';
            $sql = $sql . ' AND r.dealType = ?';
            $values[] = $typeFind1;
            $values[] = $typeFind2;
        }

        return $db->getRecords(
            'SELECT r.region, r.id
             FROM objects AS r
             ' .$sql. '
             GROUP BY r.region', $values);
    }

    public static function getObjectById($id) {
        $db = FrontendModel::getContainer()->get('database');

        return $db->getRecord(
            'SELECT r.*
             FROM objects AS r
             WHERE r.id = ?
             ORDER BY id DESC', array((int)$id));
    }

    public static function getBroker($id) {
        $db = FrontendModel::getContainer()->get('database');

        return $db->getRecord(
            'SELECT r.*
             FROM brokers AS r
             WHERE r.id = ?
             ORDER BY id DESC', array((int)$id));
    }

    public static function getBrokerByCrmId($id) {
        $db = FrontendModel::getContainer()->get('database');

        return $db->getRecord(
            'SELECT r.*
             FROM brokers AS r
             WHERE r.crmId = ?
             ORDER BY id DESC', array((int)$id));
    }

    public static function getBrokerPhoto($id) {
        $db = FrontendModel::getContainer()->get('database');

        return $db->getRecord(
            'SELECT r.*
             FROM brokers_photos AS r
             WHERE r.broker = ?
             ORDER BY id DESC', array((int)$id));
    }

    public static function getItemPhotos($id) {
        $db = FrontendModel::getContainer()->get('database');

        return $db->getRecords(
            'SELECT r.*
             FROM objects_photos AS r
             WHERE r.object = ?
             ORDER BY priority ASC', array((int)$id));
    }

    public static function insertPhoto(array $item)
    {
        $insertId = FrontendModel::getContainer()->get('database')->insert('objects_photos', $item);

        return $insertId;
    }

    public static function getMainPhoto($id) {
        $db = FrontendModel::getContainer()->get('database');

        return $db->getRecord(
            'SELECT r.*
             FROM objects_photos AS r
             WHERE r.object = ?
             ORDER BY id ASC', array((int)$id));
    }

    //Import
    public static function insert(array $item)
    {
        $insertId = FrontendModel::getContainer()->get('database')->insert('objects', $item);

        return $insertId;
    }

    public static function update(array $item)
    {
        $updateObjects = FrontendModel::getContainer()->get('database')->update(
            'objects',
            $item,
            'crmId = ?',
            array((int) $item['crmId'])
        );

        // return the new revision id
        return $updateObjects;
    }

    public static function updatePhotoPriority($id, $key)
    {
        $updateObjects = FrontendModel::getContainer()->get('database')->update(
            'objects_photos',
            array('priority' => $key),
            'crm_id = ?',
            array((int) $id)
        );

        // return the new revision id
        return $updateObjects;
    }

    public static function exists($id)
    {
        return (bool) FrontendModel::getContainer()->get('database')->getVar(
            'SELECT b.id
             FROM objects AS b
             WHERE b.id = ?',
            array((int) $id)
        );
    }

    public static function existsByCrmId($id)
    {
        return FrontendModel::getContainer()->get('database')->getVar(
            'SELECT b.id
             FROM objects AS b
             WHERE b.crmId = ?',
            array((int) $id)
        );
    }


}
