var theInt = null;
var $crosslink, $navthumb;
var curclicked = 0;

theInterval = function (cur) {
    clearInterval(theInt);

    if (typeof cur != 'undefined')
        curclicked = cur;

    $crosslink.removeClass("active-thumb");
    $navthumb.eq(curclicked).parent().addClass("active-thumb");
    $(".stripNav ul li a").eq(curclicked).trigger('click');

    theInt = setInterval(function () {
        $crosslink.removeClass("active-thumb");
        $navthumb.eq(curclicked).parent().addClass("active-thumb");
        $(".stripNav ul li a").eq(curclicked).trigger('click');
        curclicked++;
        if (6 == curclicked)
            curclicked = 0;

    }, 1000000000);
};

$(function () {

    $("#main-photo-slider").codaSlider();

    $navthumb = $(".nav-thumb");
    $crosslink = $(".cross-link");

    $navthumb
        .click(function () {
            var $this = $(this);
            theInterval($this.parent().attr('href').slice(1) - 1);
            return false;
        });

    theInterval();
});

$(".objectsListItem").click(function() {
    var id = $(this).attr('id');
    window.location.href = window.location.href + "?id=" + id ;
});

function openFilterLink(id) {
    window.location.href = window.location.href + "?id=" + id ;
}

$(".brokerBlock").click(function() {
    var id = $(this).attr('id');
    window.location.href = document.location.origin + "/komanda?id=" + id ;
});

$(".formCity").change(function() {
    var city = this.value;
    var formType = $(".formType").val();

    if(city == 0) {
        $(".formRegion").prop("disabled", true);
    }

    $.ajax(
        {
            data: {
                fork: {module: 'Objects', action: 'GetRegion'},
                city: city,
                type: formType
            },
            success: function (response) {
                $(".formRegion").html(response['data']).prop("disabled", false);
            }
        });
});

$(".formType").change(function() {
    var typeValue = this.value;

    $.ajax(
        {
            data: {
                fork: {module: 'Objects', action: 'GetExtraFields'},
                id: typeValue
            },
            success: function (response) {
                $(".searchFilterMore").html(response['data']);
            }
        });
});

$( ".filterForm" ).change(function() {
    updateFilteredData();
});

function updateFilteredData() {
    var formType = $(".formType").val();
    var filterCity = $(".formCity").val();
    var filterRegion = $(".formRegion").val();
    var filterStreet = $(".formStreet").val();
    var filterAreaFrom = $(".areaFrom").val();
    var filterAreaTo = $(".areaTo").val();
    var filterGroundAreaFrom = $(".groundAreaFrom").val();
    var filterGroundAreaTo = $(".groundAreaTo").val();
    var filterPriceFrom = $(".priceFrom").val();
    var filterPriceTo = $(".priceTo").val();
    var filterRoomsFrom= $(".roomsFrom").val();
    var filterRoomsTo = $(".roomsTo").val();
    var filterFloorFrom = $(".floorFrom").val();
    var filterFloorTo = $(".floorTo").val();
    var filterPurpose = $(".formPurpose").val();


    $.ajax(
        {
            data: {
                fork: {module: 'Objects', action: 'Filter'},
                type: formType,
                city: filterCity,
                region: filterRegion,
                street: filterStreet,
                areaFrom: filterAreaFrom,
                areaTo: filterAreaTo,
                groundAreaFrom: filterGroundAreaFrom,
                groundAreaTo: filterGroundAreaTo,
                priceFrom: filterPriceFrom,
                priceTo: filterPriceTo,
                roomsFrom: filterRoomsFrom,
                roomsTo: filterRoomsTo,
                floorFrom: filterFloorFrom,
                floorTo: filterFloorTo,
                purpose: filterPurpose
            },
            success: function (response) {
                $(".objectsItemsDiv").html(response['data']);
            }
        });
}

function lastAddedLiveFunc()
{
    var url = document.URL;
    var lastChar = url.substr(url.length - 1);

    if(lastChar == 'i') {
        var firstLoad = $('.firstLoad');

        if (firstLoad.length){
            updateFilteredData();
        }

        var lastID = $(".objectsListItem").last().attr('id');
        $('.loader').fadeIn('slow');

        $.ajax(
            {
                data: {
                    fork: {module: 'Objects', action: 'Filter'},
                    lastId: lastID
                },
                success: function (response) {
                    $(".objectsItemsDiv").append(response.data);
                }
            });
    }
    $('.loader').fadeOut('slow');

}

$(window).scroll(function(){
    if  ($(window).scrollTop() + $(window).height() == $(document).height()) {

        var noResults = $('.noresults');

        if (!noResults.length){
            lastAddedLiveFunc();
        }
    }
});