<section class="mod">
    <div class="inner">
        <div class="bd content row" {option:object}style=""{/option:object}>
            {option:objects}
                <div class="col-xs-12">
                    <div class="objectsFilter row">
                        <div class="col-sm-6 filter22">
                            <select class="form-control filterForm formType">
                                <option value="0">Tipas</option>
                                <option value="1">Butai pardavimui</option>
                                <option value="2">Namai pardavimui</option>
                                <option value="3">Sklypai pardavimui</option>
                                <option value="4">Butai nuomai</option>
                                <option value="5">Namai nuomai</option>
                                <option value="6">Patalpos pardavimui</option>
                                <option value="7">Patalpos nuomai</option>
                                {*<option value="8">Garažai pardavimui</option>*}
                                {*<option value="9">Garažai nuomai</option>*}
                                {*<option value="10">Sodai pardavimui</option>*}
                                {*<option value="11">Sodai nuomai</option>*}
                            </select>
                            <select class="form-control filterForm formCity" style="margin-top:10px;">
                                <option value="0">Miestas</option>
                                {option:cities}
                                {iteration:cities}
                                    <option value="{$cities.city}">{$cities.city}</option>
                                {/iteration:cities}
                                {/option:cities}
                            </select>
                        </div>
                        <div class="col-sm-6 filter22">
                            <select class="form-control filterForm formRegion" disabled>
                                <option value="0">Mikrorajonas</option>
                            </select>
                            <input style="margin-top:10px;" type="text" class="form-control filterForm formStreet" id="street"
                                   placeholder="Gatvė">
                        </div>
                        <div class="col-lg-6 col-xs-12 filter54">
                            <div class="searchFilterMore row ml0 mr0">
                                <div class="col-xs-6">
                                    {*Plotas*}
                                    <div class="row ml0 mr0 plot">
                                        <div class="col-md-4 filterArea filterLabel">Plotas</div>
                                        <div class="col-md-4">
                                            <input type="text" class="form-control filterForm areaFrom" id="street" placeholder="nuo">
                                        </div>
                                        <div class="col-md-4">
                                            <input type="text" class="form-control filterForm areaTo" id="street" placeholder="iki">
                                        </div>
                                        <div class="col-md-4 filterRooms filterLabel">Kambariai</div>
                                        <div class="col-md-4">
                                            <select class="form-control filterForm roomsFrom" style="margin-top:10px;">
                                                <option value="0"></option>
                                                {option:rooms}
                                                {iteration:rooms}
                                                    <option value="{$rooms.value}">{$rooms.value}</option>
                                                {/iteration:rooms}
                                                {/option:rooms}
                                            </select>
                                        </div>
                                        <div class="col-md-4">
                                            <select class="form-control filterForm roomsTo" style="margin-top:10px;">
                                                <option value="0"></option>
                                                {option:rooms}
                                                {iteration:rooms}
                                                    <option value="{$rooms.value}">{$rooms.value}</option>
                                                {/iteration:rooms}
                                                {/option:rooms}
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-xs-6">
                                    <div class="row ml0 mr0 flat">
                                        <div class="col-md-4 filterFloor filterLabel">Aukštas</div>
                                        <div class="col-md-4">
                                            <select class="form-control filterForm floorFrom">
                                                <option value="0"></option>
                                                {option:floors}
                                                {iteration:floors}
                                                    <option value="{$floors.value}">{$floors.value}</option>
                                                {/iteration:floors}
                                                {/option:floors}
                                            </select>
                                        </div>
                                        <div class="col-md-4 pr0">
                                            <select class="form-control filterForm floorTo">
                                                <option value="0"></option>
                                                {option:floors}
                                                {iteration:floors}
                                                    <option value="{$floors.value}">{$floors.value}</option>
                                                {/iteration:floors}
                                                {/option:floors}
                                            </select>
                                        </div>
                                        {*Kaina*}
                                        <div class="col-md-4 filterPrice filterLabel">Kaina</div>
                                        <div class="col-md-4">
                                            <input style="margin-top:10px;" type="text" class="form-control filterForm priceFrom" id="street" placeholder="nuo">
                                        </div>
                                        <div class="col-md-4 pr0">
                                            <input style="margin-top:10px;" type="text" class="form-control filterForm priceTo" id="street" placeholder="iki">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="objectsItemsDiv row">
                        <div class="firstLoad"></div>
                        {iteration:objects}
                            <div class="col-md-4 col-xs-6 mrgb15 houseInnerItem">
                                <div id="{$objects.id}" class="objectsListItem">
                                    <div class="objectsListItemHover"><img src="{$THEME_URL}/Core/Layout/images/dots.png"></div>
                                    <img class="objectsListImage" src="/library/external/image.php?width=248&height=159&image={$objects.photo.photo}"/>
                                    <div class="col-sm-7 street">{$objects.street}, {$objects.city}</div>
                                    <div class="col-sm-5 price"><span class="price pricePlus">{$objects.price} &#8364;</span></div>
                                </div>
                            </div>
                        {/iteration:objects}
                    </div>
                </div>
            {/option:objects}
            {option:object}
                <div class="col-lg-7 objectPhotoSlider">
                    <div class="row">
                        <div class="col-md-12">
                            <div id="main-photo-slider" class="csw">
                                <div class="panelContainer" style="width: 2350px;">
                                    {option:objectPhotos}
                                    {iteration:objectPhotos}
                                        <div class="panel" title="Panel 1">
                                            {*{$THEME_URL}/../../Files/objects/482x385/*}
                                            <div data-big="{$objectPhotos.photo}" data-fancybox-group="group-1234" class="wrapper wrapperImage">
                                                <div class="hugeImage"><img src="{$THEME_URL}/Core/Layout/images/zoom.png"></div>
                                                <img rel="gallery1" class="objectPhoto" id="" src="/library/external/image.php?width=482&height=385&cropratio=4:3&image={$objectPhotos.photo}" alt="temp"/>
                                            </div>
                                        </div>
                                    {/iteration:objectPhotos}
                                    {/option:objectPhotos}
                                </div>
                            </div>

                            <div id="movers-row" class="clearfix">
                                {option:objectPhotos}
                                {iteration:objectPhotos}
                                    <div><a href="#{$objectPhotos.number}" class="cross-link{option:objectPhotos.first} active-thumb{/option:objectPhotos.first}"><img src="/library/external/image.php?width=87&height=75&cropratio=4:3&image={$objectPhotos.photo}"
                                                                                                                                                                       class="nav-thumb" alt="temp-thumb"/></a></div>
                                {/iteration:objectPhotos}
                                {/option:objectPhotos}
                            </div>
                        </div>

                        <div class="col-md-12"><div class="descriptionBlock">{$object.description}</div></div>
                    </div>
                </div>
                <div class="col-lg-5 infoBox">
                    <div class="thatFancyTitle">
                        <span class="category">{$object.status}</span> / {$object.street}., {$object.city}
                    </div>
                    <div class="row">
                        <div class="col-xs-6 infoBoxWhoIsWho" style="padding-right: 0;">
                            <div>Savivaldybė</div>
                            <div>Rajonas</div>
                            <div>Miestas</div>
                            <div>Adresas</div>
                            {option:object.space}<div>Plotas</div>{/option:object.space}
                            {option:object.area_kitchen}<div>Virtuvės plotas</div>{/option:object.area_kitchen}
                            {option:object.groundSpace}<div>Sklypo plotas</div>{/option:object.groundSpace}
                            {option:object.year_build}<div>Statybos metai</div>{/option:object.year_build}
                            {option:object.rooms}<div>Kambarių skaičius</div>{/option:object.rooms}
                            {option:object.floor}<div>Aukštas</div>{/option:object.floor}
                            {option:!object.floor}{option:object.num_floors}<div>Aukštų skaičius</div>{/option:object.num_floors}{/option:!object.floor}
                            {option:object.type}<div>Pastato tipas</div>{/option:object.type}
                            {option:object.material}<div>Statybos tipas</div>{/option:object.material}
                            {option:object.condition}<div>Būklė</div>{/option:object.condition}
                            {option:object.building_status}<div>Statusas</div>{/option:object.building_status}
                            <div>Kaina</div>
                        </div>
                        <div class="col-xs-6 infoBoxResult">
                            <div>{$object.localgoverment}</div>
                            <div>{$object.region}</div>
                            <div>{$object.city}</div>
                            <div>{$object.street} {$object.housenumber}</div>
                            {option:object.space}<div>{$object.space} {$object.areaMeter}</div>{/option:object.space}
                            {option:object.area_kitchen}<div>{$object.area_kitchen} (m<sup>2</sup>)</div>{/option:object.area_kitchen}
                            {option:object.groundSpace}<div>{$object.groundSpace} (a)</div>{/option:object.groundSpace}
                            {option:object.year_build}<div>{$object.year_build}</div>{/option:object.year_build}
                            {option:object.rooms}<div>{$object.rooms}</div>{/option:object.rooms}
                            {option:object.floor}<div>{$object.floor} {option:object.num_floors}iš {$object.num_floors}{/option:object.num_floors}</div>{/option:object.floor}
                            {option:!object.floor}{option:object.num_floors}<div>{$object.num_floors}</div>{/option:object.num_floors}{/option:!object.floor}
                            {option:object.type}<div>{$object.type}</div>{/option:object.type}
                            {option:object.material}<div>{$object.material}</div>{/option:object.material}
                            {option:object.condition}<div>{$object.condition}</div>{/option:object.condition}
                            {option:object.building_status}<div>{$object.building_status}</div>{/option:object.building_status}
                            <div class="price"><b>{$object.price} &#8364;</b></div>
                        </div>
                        {option:features}
                            <div class="features col-xs-12">
                                <div class="row">
                                    <div class="featuresHeader col-lg-3">
                                        Ypatybės
                                    </div>
                                    <div class="col-lg-9">
                                        {iteration:features}
                                            <div class="featuresInner">{$features.value}</div>
                                        {/iteration:features}
                                    </div>
                                </div>
                            </div>
                        {/option:features}
                    </div>
                </div>
                <div id="{$broker.id}" class="col-lg-5 brokerBlock">
                    <div class="row">
                        <div class="col-xs-7 objectBrokerBlock">
                            <div class="objectBrokerName">{$broker.name}</div>
                            <div class="objectBrokerPhone">{$broker.phone}</div>
                            <div class="objectBrokerEmail">{$broker.email}</div>
                        </div>
                        <div class=" col-xs-5 objectBrokerBlock">
                            <div class="objectBrokerPhoto"><img src="{$THEME_URL}/../../Files/brokers/128x128/{$brokerPhoto.photo}"/></div>
                        </div>
                    </div>
                </div>
            {/option:object}
        </div>
    </div>
    </div>
</section>