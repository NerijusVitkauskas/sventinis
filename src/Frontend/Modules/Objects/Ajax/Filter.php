<?php

namespace Frontend\Modules\Objects\Ajax;

/*
 * This file is part of Fork CMS.
 *
 * For the full copyright and license information, please view the license
 * file that was distributed with this source code.
 */

use Frontend\Core\Engine\Base\AjaxAction as FrontendBaseAJAXAction;
use Frontend\Core\Engine\Model as FrontendModel;
use Frontend\Modules\Objects\Engine\Model as FrontendObjectsModel;
use Frontend\Modules\Objects\Engine\Helper as FrontendObjectsHelper;


class Filter extends FrontendBaseAJAXAction
{
    private $items = array();
    /**
     * Execute the action
     */
    public function execute()
    {
        parent::execute();
        $lastId = \SpoonFilter::getPostValue('lastId', null, '');
        $type = \SpoonFilter::getPostValue('type', null, '');
        $city = \SpoonFilter::getPostValue('city', null, '');
        $region = \SpoonFilter::getPostValue('region', null, '');
        $street = \SpoonFilter::getPostValue('street', null, '');
        $areaFrom = \SpoonFilter::getPostValue('areaFrom', null, '');
        $areaTo = \SpoonFilter::getPostValue('areaTo', null, '');
        $groundAreaFrom = \SpoonFilter::getPostValue('groundAreaFrom', null, '');
        $groundAreaTo = \SpoonFilter::getPostValue('groundAreaTo', null, '');
        $priceFrom = \SpoonFilter::getPostValue('priceFrom', null, '');
        $priceTo = \SpoonFilter::getPostValue('priceTo', null, '');
        $roomsFrom = \SpoonFilter::getPostValue('roomsFrom', null, '');
        $roomsTo = \SpoonFilter::getPostValue('roomsTo', null, '');
        $floorFrom = \SpoonFilter::getPostValue('floorFrom', null, '');
        $floorTo = \SpoonFilter::getPostValue('floorTo', null, '');
        $purpose = \SpoonFilter::getPostValue('purpose', null, '');

        $typeFind1 = $typeFind2 = NULL;

        if($type) {
            if ($type == 1) {
                $typeFind1 = 'apartment';
                $typeFind2 = 'sale';
            }
            if ($type == 2) {
                $typeFind1 = 'house';
                $typeFind2 = 'sale';
            }
            if ($type == 3) {
                $typeFind1 = 'land';
                $typeFind2 = 'sale';
            }
            if ($type == 4) {
                $typeFind1 = 'apartment';
                $typeFind2 = 'rent';
            }
            if ($type == 5) {
                $typeFind1 = 'house';
                $typeFind2 = 'rent';
            }
            if ($type == 6) {
                $typeFind1 = 'premises';
                $typeFind2 = 'sale';
            }
            if ($type == 7) {
                $typeFind1 = 'premises';
                $typeFind2 = 'rent';
            }
            if ($type == 8) {
                $typeFind1 = 'garage';
                $typeFind2 = 'sale';
            }
            if ($type == 9) {
                $typeFind1 = 'garage';
                $typeFind2 = 'rent';
            }

            if ($type == 10) {
                $typeFind1 = 'garden';
                $typeFind2 = 'sale';
            }

            if ($type == 11) {
                $typeFind1 = 'garden';
                $typeFind2 = 'rent';
            }
        }

        $fields = '';

        $items = FrontendObjectsModel::getObjectsByFilter($lastId, $type, $street, $typeFind1, $typeFind2, $city, $region, $areaFrom, $areaTo, $groundAreaFrom, $groundAreaTo, $priceFrom, $priceTo, $roomsFrom, $roomsTo, $floorFrom, $floorTo, $purpose);
        foreach($items as $key=>$item) {
            $photo = FrontendObjectsModel::getMainPhoto($item['id']);
            $price = number_format($item['price']);
            $price = preg_replace('/\,/', ' ', $price);

            $fields = $fields . '<div class="col-md-4 col-xs-6 mrgb15 houseInnerItem">
                            <div class="objectsListItem" id="' . $item['id'] . '" onclick="openFilterLink(' . $item['id'] . ');">
                                <div class="objectsListItemHover"><img src="/src/Frontend/Themes/creston/Core/Layout/images/dots.png"></div>
                                <img class="objectsListImage" src="/library/external/image.php?width=248&amp;height=159&amp;image=' . $photo['photo'] . '">
                                <div class="col-sm-7 street">' . $item['street'] . ', ' . $item['city'] . '</div>
                                <div class="col-sm-5 price"><span class="price pricePlus">' . $price . ' €</span></div>
                            </div>
                        </div>';
        }

        $countItems = count($items);
        if($countItems < 15) {
            $fields = $fields .  '<div class="noresults"></div>';
        }

        $this->output(self::OK, $fields);

    }
}
