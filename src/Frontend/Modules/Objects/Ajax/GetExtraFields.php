<?php

namespace Frontend\Modules\Objects\Ajax;

use Frontend\Core\Engine\Base\AjaxAction as FrontendBaseAJAXAction;
use Frontend\Core\Engine\Model as FrontendModel;
use Frontend\Modules\Objects\Engine\Helper as FrontendObjectsHelper;


class GetExtraFields extends FrontendBaseAJAXAction
{
    public function execute()
    {
        $floors = FrontendObjectsHelper::getNumbers();
        $rooms = FrontendObjectsHelper::getNumbers(10);

        parent::execute();
        $id = \SpoonFilter::getPostValue('id', null, '');

        $field = '<div class="col-xs-6"><div class="row ml0 mr0 plot">';

        //Plotas
        $field = $field . '
                                <div class="col-md-4 filterArea filterLabel">Plotas</div>
                                <div class="col-md-4">
                                    <input onchange="updateFilteredData();" type="text" class="form-control filterForm areaFrom" id="street" placeholder="nuo">
                                </div>
                                <div class="col-md-4">
                                    <input onchange="updateFilteredData();" type="text" class="form-control filterForm areaTo" id="street" placeholder="iki">
                                </div>';

        //Kambariai
        if($id != 3) {
            $field = $field . '
                                    <div class="col-md-4 filterRooms filterLabel">Kambariai</div>
                                    <div class="col-md-4">
                                        <select onchange="updateFilteredData();" class="form-control filterForm roomsFrom" style="margin-top:10px;">
                                            <option value="0"></option>';
            foreach($rooms as $room) {
                $field = $field . '<option value="' . $room['value'] . '">' . $room['value'] . '</option>';
            }
            $field = $field . '</select>
                                    </div>
                                    <div class="col-md-4">
                                        <select onchange="updateFilteredData();" class="form-control filterForm roomsTo" style="margin-top:10px;">
                                            <option value="0"></option>';
            foreach($rooms as $room) {
                $field = $field . '<option value="' . $room['value'] . '">' . $room['value'] . '</option>';
            }
            $field = $field . '</select>
                                    </div>';
        }
        $field = $field . '</div></div> <div class="col-xs-6"><div class="row ml0 mr0 flat">';


        //Aukštas
        if($id == 1 || $id==4 || $id == 0 || $id == 7 || $id == 6) {
            $field = $field . '<div class="col-md-4 filterFloor filterLabel">Aukštas</div>
                               <div class="col-md-4">
                                    <select onchange="updateFilteredData();" class="form-control filterForm floorFrom">
                                        <option value="0"></option>';
            foreach($floors as $floor) {
                $field = $field . '     <option value="' . $floor['value'] . '">' . $floor['value'] . '</option>';
            }
            $field = $field . '     </select>
                                </div>
                                <div class="col-md-4 pr0">
                                    <select onchange="updateFilteredData();" class="form-control filterForm floorTo">
                                        <option value="0"></option>';
            foreach($floors as $floor) {
                $field = $field . '     <option value="' . $floor['value'] . '">' . $floor['value'] . '</option>';
            }
            $field = $field . '     </select>
                                </div>';
        }

        //Sklypo pl
        if($id == 2 || $id==5) {
            $field = $field . '<div class="col-md-4 filterArea filterLabel">Sklypo pl.</div>
                               <div class="col-md-4">
                                    <input onchange="updateFilteredData();" type="text" class="form-control filterForm groundAreaFrom" id="groundarea" placeholder="nuo">
                                </div>
                                <div class="col-md-4 pr0">
                                    <input onchange="updateFilteredData();" type="text" class="form-control filterForm groundAreaTo" id="groundarea" placeholder="iki">
                                </div>';
        }

        //Paskirtis
        if($id == 3) {
            $field = $field . '<div class="col-md-4 filterReason filterLabel">Paskirtis</div>
                                <div class="col-md-8 pr0">
                                    <select onchange="updateFilteredData();" class="form-control filterForm formPurpose">
                                        <option value="0"></option>
                                        <option value="residential">Namų valda</option>
                                        <option value="residential">Gyvenamoji statyba</option>
                                        <option value="other">Daugiaaukštė statyba</option>
                                        <option value="arable">Žemės ūkio</option>
                                        <option value="commercial">Komercinė</option>
                                        <option value="other">Rekreacinė</option>
                                        <option value="other">Kolektyvinis sodas</option>
                                        <option value="other">Nėra duomenų</option>
                                    </select>
                                </div>';
        }

        //Kaina
        $field = $field . '<div class="col-md-4 filterPrice filterLabel">Kaina</div>
                                <div class="col-md-4">
                                    <input onchange="updateFilteredData();" style="margin-top:10px;" type="text" class="form-control filterForm priceFrom" id="street" placeholder="nuo">
                                </div>
                                <div class="col-md-4 pr0">
                                    <input onchange="updateFilteredData();" style="margin-top:10px;" type="text" class="form-control filterForm priceTo" id="street" placeholder="iki">
                                </div>
                            </div></div>';

       $this->output(self::OK, $field);
    }
}
