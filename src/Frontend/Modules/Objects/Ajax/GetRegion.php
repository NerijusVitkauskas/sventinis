<?php

namespace Frontend\Modules\Objects\Ajax;

use Frontend\Core\Engine\Base\AjaxAction as FrontendBaseAJAXAction;
use Frontend\Core\Engine\Model as FrontendModel;
use Frontend\Modules\Objects\Engine\Helper as FrontendObjectsHelper;


class GetRegion extends FrontendBaseAJAXAction
{
    public function execute()
    {
        parent::execute();
        $city = \SpoonFilter::getPostValue('city', null, '');
        $type = \SpoonFilter::getPostValue('type', null, '');

        $regions = FrontendObjectsHelper::getRegions($city, $type);

        $data = '<option value="0">Mikrorajonas</option>';
        foreach ($regions as $region) {
            $data = $data . '<option value="' . $region['region'] . '">' . $region['region'] . '</option>';
        }
        $this->output(self::OK, $data);
    }
}
