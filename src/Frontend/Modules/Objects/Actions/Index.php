<?php

namespace Frontend\Modules\Objects\Actions;

use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\HttpFoundation\File\File;

use Frontend\Core\Engine\Base\Block as FrontendBaseBlock;
use Frontend\Core\Engine\Model as FrontendModel;
use Frontend\Modules\Objects\Engine\Model as FrontendObjectsModel;
use Frontend\Modules\Objects\Engine\Helper as FrontendObjectsHelper;

class Index extends FrontendBaseBlock
{
    /**
     * @var    array
     */
    private $items = array();
    private $item = array();
    private $broker = array();
    private $brokerPhoto = array();
    private $itemPhotos = array();
    private $cities = array();
    private $features = array();


    /**
     * Execute the extra
     */
    public function execute()
    {
        parent::execute();
        $this->getData();
        $this->loadTemplate();
        $this->parse();
    }

    private function getData() {
        $import = \SpoonFilter::getGetValue('import', null, '');
        if($import) {
            $this->importObjects();
        }


        $id = \SpoonFilter::getGetValue('id', null, '');
        if((int)$id) {
            $object = FrontendObjectsHelper::changeValue(FrontendObjectsModel::getObjectById($id));

            $modifiefItem = array();
            foreach($object as $key=>$item) {
                if(is_numeric($item) && $item == 0) {
                    $item = NULL;
                }
                $modifiefItem[$key] = $item;
                $objectType = FrontendObjectsHelper::objectTypes($object['objectType']);
                $dealType = FrontendObjectsHelper::objectTypes($object['dealType']);
                $modifiefItem['status'] = $objectType.$dealType;
                if($objectType == 'Sklypas') {
                    $areaMeter = '(a)';
                } else {
                    $areaMeter = '(m<sup>2</sup>)';
                }
                $modifiefItem['areaMeter'] = $areaMeter;
            }

            if (isset($object['features']) && !empty($object['features'])) {
                $features = unserialize($object['features']);
                foreach ($features as $featureKey => $feature) {
                    if(empty($feature)) {
                        $this->features[]['value'] = FrontendObjectsHelper::extraFeatures($featureKey);
                    }
                }
            }

            $this->item = $modifiefItem;
            $itemPhotos = FrontendObjectsModel::getItemPhotos($id);
            $this->broker = FrontendObjectsModel::getBroker($this->item['broker']);
            $brokerPhoto = FrontendObjectsModel::getBrokerPhoto($this->item['broker']);
            if ($brokerPhoto) {
                $this->brokerPhoto = $brokerPhoto;
            }

            if ($itemPhotos) {
                foreach($itemPhotos as $key => $value) {
                    $itemPhotos[$key]['number'] = $key + 1;
                }
                $itemPhotos[0]['first'] = 'true';
                $this->itemPhotos = $itemPhotos;
            }
        } else {
            $this->items = FrontendObjectsModel::getObjects();
            foreach($this->items as $key=>$item) {
                $this->items[$key]['photo'] = FrontendObjectsModel::getMainPhoto($item['id']);
                $price = number_format($item['price']);
                $price = preg_replace('/\,/', ' ', $price);
                $this->items[$key]['price'] = $price;
                $this->cities = FrontendObjectsHelper::getCities();
            }
        }
    }

    private function parse()
    {
        $this->addJS("jquery-easing-1.3.pack.js");
        $this->addJS("jquery-easing-compatibility.1.2.pack.js");
        $this->addJS("coda-slider.1.1.1.pack.js");
        $this->addCSS("sliderstyle.css");

        $this->tpl->assign('objects', $this->items);
        $this->tpl->assign('object', $this->item);
        $this->tpl->assign('features', $this->features);
        $this->tpl->assign('broker', $this->broker);
        $this->tpl->assign('brokerPhoto', $this->brokerPhoto);
        $this->tpl->assign('objectPhotos', $this->itemPhotos);
        $this->tpl->assign('cities', $this->cities);
        $this->tpl->assign('floors', FrontendObjectsHelper::getNumbers());
        $this->tpl->assign('rooms', FrontendObjectsHelper::getNumbers(10));
    }

    private function importObjects()
    {
        $objectsIdsArray = array();

        $xml = simplexml_load_file('http://crm.creston.lt/uploads/object/export/xml/creston/export.xml');
        $objects = json_decode( json_encode($xml) , 1);

        foreach($objects['object'] as $object) {
            $objectInfo = array();
            $objectId = $object['object_id'];
            $objectsIdsArray[] = $objectId;
            $objectType = $object['object_type'];
            $dealType = $object['deal_type'];

            $expiredDate = strtotime($object['expiresDate']);

            $images = array();
            $images = $object['images'];

            $objectInfo['crmId'] = $objectId;
            $broker = FrontendObjectsModel::getBrokerByCrmId($object['broker']['broker_id']);
            $brokerId = 3;
            if(isset($broker['id'])) {
                $brokerId = $broker['id'];
            }
            $objectInfo['broker'] = $brokerId;
            $objectInfo['expiredDate'] = $expiredDate;
            $objectInfo['price'] = $object['general']['price'];

            $objectInfo['objectType'] = $objectType;
            $objectInfo['dealType'] = $dealType;

            $objectInfo['street'] = $object['location_address']['street'];

            if(isset($object['location_address']['house_number'])){
                $objectInfo['housenumber'] = $object['location_address']['house_number'];
            }

            if(isset($object['location_address']['county'])){
                $objectInfo['city'] = $object['location_address']['county'];
            }
            if(isset($object['location_address']['city'])){
                $objectInfo['region'] = $object['location_address']['city'];
            }

            if(isset($object['location_address']['parish'])){
                $objectInfo['localgoverment'] = $object['location_address']['parish'];
            }

            if(isset($object['general']['condition'])){
                $objectInfo['condition'] = $object['general']['condition'];
            }

            if(isset($object['general']['year_build'])){
                $objectInfo['year_build'] = $object['general']['year_build'];
            }

            if(isset($object['general']['area_total'])){
                $objectInfo['space'] = $object['general']['area_total'];
            }

            if(isset($object['general']['area_ground'])){
                $objectInfo['groundSpace'] = $object['general']['area_ground'];
            }

            if(isset($object['general']['num_rooms'])){
                $objectInfo['rooms'] = $object['general']['num_rooms'];
            }

            if(isset($object['general']['building_structure'])){
                $objectInfo['material'] = $object['general']['building_structure'];
            }

            if(isset($object['general']['building_status'])){
                $objectInfo['building_status'] = $object['general']['building_status'];
            }

            if(isset($object['general']['area_kitchen'])){
                $objectInfo['area_kitchen'] = $object['general']['area_kitchen'];
            }

            if(isset($object['general']['this_floor'])){
                $objectInfo['floor'] = $object['general']['this_floor'];
            }

            if(isset($object['general']['num_floors'])){
                $objectInfo['num_floors'] = $object['general']['num_floors'];
            }

            if(isset($object['general']['land_purpose'])){
                if(is_array($object['general']['land_purpose'])) {
                    $objectInfo['land_purpose'] = implode(",", $object['general']['land_purpose']);
                } else {
                    $objectInfo['land_purpose'] = $object['general']['land_purpose'];
                }
            }

            if(isset($object['detail_info']['info'])){
                $objectInfo['description'] = $object['detail_info']['info'];
            }

            if(isset($object['features']) && !empty($object['features'])){
                $objectInfo['features'] = serialize($object['features']);
            }
            $exist = FrontendObjectsModel::existsByCrmId($objectId);

            if((bool)$exist) {
                $update = FrontendObjectsModel::update($objectInfo);
                if ($images) {
                    $objectsPhotos = FrontendObjectsModel::getItemPhotos($exist);
                    $objectsPhotosUrls = array();
                    $objectsPhotosLeft = array();
                    foreach($objectsPhotos as $objPhoto) {
                        $objectsPhotosUrls[] = $objPhoto['image_url'];
                        $objectsPhotosLeft[$objPhoto['crm_id']] = $objPhoto;
                    }
                    $imageKey = 0;
                    if (!isset($images['image'][0])) {
                        $foreachBy = $images;
                    } else {
                        $foreachBy = $images['image'];
                    }
                    foreach ($foreachBy as $imageValue) {
                        if($imageValue) {
                            unset($objectsPhotosLeft[$imageValue['image_id']]);
                            $imageKey++;
                            $imageUrl = $imageValue['imageURI'];

                            $thisImage = array();
                            $imagePath = FRONTEND_FILES_PATH . '/objects';
                            $imagePathForDb = '/src/Frontend/Files/objects';
                            preg_match("/[^\/]+$/", $imageUrl, $matches);
                            $imageName = $matches[0];

                            if (!$objectsPhotos || !in_array($imageUrl, $objectsPhotosUrls)) {
                                $fs = new Filesystem();
                                if ($fs->exists($imagePath . '/source/' . $imageName)) {
                                    $i = 1;
                                    while ($fs->exists($imagePath . '/source/' . $i . $imageName)) {
                                        $i = rand();
                                    }
                                    $imageName = $i . $imageName;
                                }

                                $content = file_get_contents($imageUrl);

                                $fp = fopen($imagePath . '/source/' . $imageName, "w");
                                fwrite($fp, $content);
                                fclose($fp);
                                $thisImage['object'] = $exist;
                                $thisImage['priority'] = $imageKey;
                                $thisImage['crm_id'] = $imageValue['image_id'];
                                $thisImage['photo'] = $imagePathForDb . '/source/' . $imageName;
                                $thisImage['real_path'] = $imagePath . '/source/' . $imageName;
                                $thisImage['image_url'] = $imageUrl;
                                FrontendObjectsModel::insertPhoto($thisImage);
                            } else {
                                FrontendObjectsModel::updatePhotoPriority($imageValue['image_id'], $imageKey);
                            }
                        }
                    }

                    if($objectsPhotosLeft) {
                        foreach ($objectsPhotosLeft as $photoLeft) {
                            FrontendObjectsModel::deletePhotoByCrmId($photoLeft['crm_id']);
                            unlink($photoLeft['real_path']);
                        }
                    }
                }
            } else {
                $innerId = FrontendObjectsModel::insert($objectInfo);
                if($images) {
                    $imageKey = 0;

                    if(!isset($images['image'][0])) {
                        $foreachBy = $images;
                    } else {
                        $foreachBy = $images['image'];
                    }

                    foreach ($foreachBy as $imageValue) {
                        $imageKey++;
                        $imageUrl = $imageValue['imageURI'];
                        $thisImage = array();
                        $imagePath = FRONTEND_FILES_PATH . '/objects';
                        $imagePathForDb = '/src/Frontend/Files/objects';
                        preg_match("/[^\/]+$/", $imageUrl, $matches);
                        $imageName = $matches[0];

                        $fs = new Filesystem();
                        if ($fs->exists($imagePath . '/source/' . $imageName)) {
                            $i = 1;
                            while ($fs->exists($imagePath . '/source/' . $i. $imageName)) {
                                $i = rand();
                            }
                            $imageName = $i. $imageName;
                        }

                        $content = file_get_contents($imageUrl);

                        $fp = fopen($imagePath . '/source/' . $imageName, "w");
                        fwrite($fp, $content);
                        fclose($fp);
                        $thisImage['object'] = $innerId;
                        $thisImage['priority'] = $imageKey;
                        $thisImage['crm_id'] = $imageValue['image_id'];
                        $thisImage['photo'] = $imagePathForDb . '/source/' . $imageName;
                        $thisImage['real_path'] = $imagePath . '/source/' . $imageName;
                        $thisImage['image_url'] = $imageUrl;

                        FrontendObjectsModel::insertPhoto($thisImage);
                    }
                }
            }
        }

        $this->deleteOldObjects($objectsIdsArray);

        var_dump("Baigta"); die();
    }

    private function deleteOldObjects($objectsIdsArray)
    {
        $allObjectsCrmIds = FrontendObjectsModel::getAllObjectsCrmIds();
        foreach($allObjectsCrmIds as $crmId) {
            if (!in_array($crmId['crmId'], $objectsIdsArray)) {
                FrontendObjectsModel::deleteObject($crmId['crmId']);
                $objectsPhotos = FrontendObjectsModel::getItemPhotos($crmId['id']);

                foreach($objectsPhotos as $photo) {
                    FrontendObjectsModel::deletePhoto($crmId['id']);
                    unlink($photo['real_path']);
                }
            }
        }
    }
}
