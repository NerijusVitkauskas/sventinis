<?php

namespace Frontend\Modules\Quotation\Engine;

use Frontend\Core\Engine\Model as FrontendModel;
use Frontend\Core\Engine\Navigation as FrontendNavigation;
use Frontend\Core\Engine\Url as FrontendURL;

class Model
{
    public static function getQuotations() {
        $db = FrontendModel::getContainer()->get('database');

        return (array) $db->getRecords(
            'SELECT r.*
             FROM quotation AS r
             ORDER BY id DESC');
    }
}
