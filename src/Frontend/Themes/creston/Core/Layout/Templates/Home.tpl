{include:Core/Layout/Templates/Head.tpl}
	{include:Core/Layout/Templates/Cookies.tpl}

	{* General Javascript *}
	{iteration:jsFiles}
		<script src="{$jsFiles.file}"></script>
	{/iteration:jsFiles}

	{* Site wide HTML *}
	{$siteHTMLFooter}
</body>
</html>
