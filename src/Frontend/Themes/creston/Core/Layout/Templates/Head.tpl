<!DOCTYPE html>
<!--[if lt IE 7 ]> <html lang="{$LANGUAGE}" class="ie6"> <![endif]-->
<!--[if IE 7 ]> <html lang="{$LANGUAGE}" class="ie7"> <![endif]-->
<!--[if IE 8 ]> <html lang="{$LANGUAGE}" class="ie8"> <![endif]-->
<!--[if IE 9 ]> <html lang="{$LANGUAGE}" class="ie9"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--> <html lang="{$LANGUAGE}"> <!--<![endif]-->
<head>
	{* Meta *}
	<meta charset="utf-8" />
	<meta name="generator" content="Fork CMS" />
	{$meta}
	{$metaCustom}
    <meta name="viewport" content="width=device-width, initial-scale=1">

	<title>{$pageTitle}</title>

    <link href='http://fonts.googleapis.com/css?family=Roboto+Slab&subset=latin-ext' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Roboto+Slab:300&subset=latin,latin-ext' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Roboto+Slab:100&subset=latin,latin-ext' rel='stylesheet' type='text/css'>

    {* Favicon and Apple touch icon *}
	<link rel="shortcut icon" href="{$THEME_URL}/favicon.ico" />
	<link rel="apple-touch-icon" href="{$THEME_URL}/apple-touch-icon.png" />

	{* Windows 8 tile *}
	<meta name="application-name" content="{$siteTitle}"/>
	<meta name="msapplication-TileColor" content="#3380aa"/>
	<meta name="msapplication-TileImage" content="{$THEME_URL}/tile.png"/>

	{* Stylesheets *}
	{iteration:cssFiles}
		<link rel="stylesheet" href="{$cssFiles.file}" />
	{/iteration:cssFiles}

    {*<link rel="stylesheet" href="{$THEME_URL}/Core/Layout/Css/style.css" />*}
    {*<link rel="stylesheet" href="{$THEME_URL}/Core/Layout/Css/creston.css" />*}
    <link rel="stylesheet" href="{$THEME_URL}/Core/Layout/Css/custom.css" />
	<!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
	<!--[if lt IE 9]>
		<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
	<![endif]-->

	{* Site wide HTML *}
	{$siteHTMLHeader}
    <script src="{$THEME_URL}/Core/Js/modernizr.js"></script>
</head>
<body id="page">
<ul class="bodyBackground"></ul>
<div class="loader"></div>
<ul class="cb-slideshow">
    {option:quotation}
    {iteration:quotation}
        <li><span></span><div><h3>&bdquo;{$quotation.quotation}&ldquo; - {$quotation.author}</h3></div></li>
    {/iteration:quotation}
    {/option:quotation}
</ul>

<div class="main-menu">
    <div class="main-menu-inner">
        <a href="/"><img class="header-image" src="{$THEME_URL}/Core/Layout/images/logo.png"></a>
        <div class="header-logo">
        </div>
        {* Navigation *}
        <div class="menu-buttons">
            {$var|getnavigation:'page':0:2}
        </div>
        <div class="searchMenu">
            <li><a href="/objektai">Objektų paieška</a></li>
            <img class="searchMenuImg" src="{$THEME_URL}/Core/Layout/images/search.png">
        </div>
        <div class="socialMedia">
            <a href="{$lblSocialMediaFacebook}" class="socialMediaImage socialMediaFacebook"></a>
            <a href="{$lblSocialMediaIn}" class="socialMediaImage socialMediaIn"></a>
        </div>
    </div>
</div>