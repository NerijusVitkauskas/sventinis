<?php

namespace Backend\Modules\Objects\Engine;

use Symfony\Component\Filesystem\Filesystem;
use Backend\Core\Engine\Authentication as BackendAuthentication;
use Backend\Core\Engine\Language as BL;
use Backend\Core\Engine\Model as BackendModel;

class Model
{
    const QRY_DATAGRID_OBJECTS =
        'SELECT *
         FROM objects AS b
         ORDER BY b.id ASC';

    public static function insert(array $item)
    {
        $insertId = BackendModel::getContainer()->get('database')->insert('objects', $item);
        BackendModel::invalidateFrontendCache('Objects', BL::getWorkingLanguage());

        return $insertId;
    }

    public static function exists($id)
    {
        return (bool) BackendModel::getContainer()->get('database')->getVar(
            'SELECT b.id
             FROM objects AS b
             WHERE b.id = ?',
            array((int) $id)
        );
    }

    public static function get($id)
    {
        return (array) BackendModel::getContainer()->get('database')->getRecord(
            'SELECT b.*
             FROM objects AS b
             WHERE b.id = ?',
            array((int) $id)
        );
    }

    public static function update(array $item)
    {
        $updateObjects = BackendModel::getContainer()->get('database')->update(
            'objects',
            $item,
            'id = ?',
            array((int) $item['id'])
        );

        // invalidate the cache for blog
        BackendModel::invalidateFrontendCache('Objects', BL::getWorkingLanguage());

        // return the new revision id
        return $updateObjects;
    }

    public static function getObjects() {
        $db = BackendModel::getContainer()->get('database');

        return (array) $db->getRecords(
            'SELECT r.*
             FROM objects AS r
             ORDER BY id DESC');
    }
}
