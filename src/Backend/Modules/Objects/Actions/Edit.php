<?php

namespace Backend\Modules\Objects\Actions;

use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\HttpFoundation\File\File;

use Backend\Core\Engine\Base\ActionEdit as BackendBaseActionEdit;
use Backend\Core\Engine\Authentication as BackendAuthentication;
use Backend\Core\Engine\Model as BackendModel;
use Backend\Core\Engine\Form as BackendForm;
use Backend\Core\Engine\Language as BL;
use Backend\Modules\Objects\Engine\Model as BackendObjectsModel;

class Edit extends BackendBaseActionEdit
{
    public $record;

    public function execute()
    {
        // get parameters
        $this->id = $this->getParameter('id', 'int');

        // does the item exists
        if ($this->id !== null && BackendObjectsModel::exists($this->id)) {
            parent::execute();

            $this->loadData();
            $this->loadForm();
            $this->validateForm();
            $this->parse();
            $this->display();
        } else {
            $this->redirect(BackendModel::createURLForAction('Index') . '&error=non-existing');
        }
    }

    private function loadData() {
        $this->record = (array) BackendObjectsModel::get($this->id);
    }
    /**
     * Load the form
     */
    private function loadForm()
    {
        // create form
        $this->frm = new BackendForm('edit');

        // create elements
        $this->frm->addText('localgoverment', $this->record['localgoverment'], null, 'inputText title', 'inputTextError title');
        $this->frm->addText('city', $this->record['city'], null, 'inputText title', 'inputTextError title');
        $this->frm->addText('region', $this->record['region'], null, 'inputText title', 'inputTextError title');
        $this->frm->addText('street', $this->record['street'], null, 'inputText title', 'inputTextError title');
        $this->frm->addText('rooms', $this->record['rooms'], null, 'inputText title', 'inputTextError title');
        $this->frm->addText('floor', $this->record['floor'], null, 'inputText title', 'inputTextError title');
        $this->frm->addText('type', $this->record['type'], null, 'inputText title', 'inputTextError title');
        $this->frm->addText('material', $this->record['material'], null, 'inputText title', 'inputTextError title');
        $this->frm->addText('condition', $this->record['condition'], null, 'inputText title', 'inputTextError title');
        $this->frm->addText('heating', $this->record['heating'], null, 'inputText title', 'inputTextError title');
        $this->frm->addText('price', $this->record['price'], null, 'inputText title', 'inputTextError title');
        $this->frm->addEditor('description', $this->record['description']);
    }

    /**
     * Parse the form
     */
    protected function parse()
    {
        parent::parse();

        // get url
        $url = BackendModel::getURLForBlock($this->URL->getModule(), 'detail');
        $url404 = BackendModel::getURL(404);

        // parse additional variables
        if ($url404 != $url) {
            $this->tpl->assign('detailURL', SITE_URL . $url);
        }

        // assign the active record and additional variables
        $this->tpl->assign('item', $this->record);
    }

    /**
     * Validate the form
     */
    private function validateForm()
    {
        // is the form submitted?
        if ($this->frm->isSubmitted()) {
            // cleanup the submitted fields, ignore fields that were added by hackers
            $this->frm->cleanupFields();

            // validate fields
            $this->frm->getField('localgoverment')->isFilled(BL::err('LocalgovermentIsRequired'));
            $this->frm->getField('city')->isFilled(BL::err('CityIsRequired'));
            $this->frm->getField('region')->isFilled(BL::err('RegionIsRequired'));
            $this->frm->getField('street')->isFilled(BL::err('StreetIsRequired'));
            $this->frm->getField('rooms')->isFilled(BL::err('RoomsIsRequired'));
            $this->frm->getField('floor')->isFilled(BL::err('FlorIsRequired'));
            $this->frm->getField('type')->isFilled(BL::err('TypeIsRequired'));
            $this->frm->getField('material')->isFilled(BL::err('MaterialIsRequired'));
            $this->frm->getField('condition')->isFilled(BL::err('ConditionIsRequired'));
            $this->frm->getField('heating')->isFilled(BL::err('HeatingIsRequired'));
            $this->frm->getField('price')->isFilled(BL::err('PriceIsRequired'));
            $this->frm->getField('description')->isFilled(BL::err('DescriptionIsRequired'));

            // no errors?
            if ($this->frm->isCorrect()) {
                // build item
                $item['id'] = $this->id;

                $item['localgoverment'] = $this->frm->getField('localgoverment')->getValue();
                $item['city'] = $this->frm->getField('city')->getValue();
                $item['region'] = $this->frm->getField('region')->getValue();
                $item['street'] = $this->frm->getField('street')->getValue();
                $item['rooms'] = $this->frm->getField('rooms')->getValue();
                $item['floor'] = $this->frm->getField('floor')->getValue();
                $item['type'] = $this->frm->getField('type')->getValue();
                $item['material'] = $this->frm->getField('material')->getValue();
                $item['condition'] = $this->frm->getField('condition')->getValue();
                $item['heating'] = $this->frm->getField('heating')->getValue();
                $item['price'] = $this->frm->getField('price')->getValue();
                $item['description'] = $this->frm->getField('description')->getValue(true);

                BackendObjectsModel::update($item);

                $redirectUrl = BackendModel::createURLForAction('Edit') .
                    '&report=saved&var=' . urlencode($item['street']) .
                    '&id=' . $item['id'] . '&draft=' . $item['id'] .
                    '&highlight=row-' . $item['id'];

                // everything is saved, so redirect to the overview
                $this->redirect($redirectUrl);
            }
        }
    }
}
