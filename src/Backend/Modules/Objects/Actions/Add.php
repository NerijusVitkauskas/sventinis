<?php

namespace Backend\Modules\Objects\Actions;

use Backend\Core\Engine\Base\ActionAdd as BackendBaseActionAdd;
use Backend\Core\Engine\Authentication as BackendAuthentication;
use Backend\Core\Engine\Form as BackendForm;
use Backend\Core\Engine\Language as BL;
use Backend\Core\Engine\Model as BackendModel;
use Backend\Modules\Objects\Engine\Model as BackendObjectsModel;


class Add extends BackendBaseActionAdd
{
    /**
     * Execute the action
     */
    public function execute()
    {
        parent::execute();

        $this->loadForm();
        $this->validateForm();

        $this->parse();
        $this->display();
    }

    /**
     * Load the form
     */
    private function loadForm()
    {
        // create form
        $this->frm = new BackendForm('add');

        $this->frm->addText('localgoverment', null, null, 'inputText title', 'inputTextError title');
        $this->frm->addText('city', null, null, 'inputText title', 'inputTextError title');
        $this->frm->addText('region', null, null, 'inputText title', 'inputTextError title');
        $this->frm->addText('street', null, null, 'inputText title', 'inputTextError title');
        $this->frm->addText('rooms', null, null, 'inputText title', 'inputTextError title');
        $this->frm->addText('floor', null, null, 'inputText title', 'inputTextError title');
        $this->frm->addText('type', null, null, 'inputText title', 'inputTextError title');
        $this->frm->addText('material', null, null, 'inputText title', 'inputTextError title');
        $this->frm->addText('condition', null, null, 'inputText title', 'inputTextError title');
        $this->frm->addText('heating', null, null, 'inputText title', 'inputTextError title');
        $this->frm->addText('price', null, null, 'inputText title', 'inputTextError title');
        $this->frm->addEditor('description');
    }

    /**
     * Parse the page
     */
    protected function parse()
    {
        parent::parse();

        // get url
        $url = BackendModel::getURLForBlock($this->URL->getModule(), 'detail');
        $url404 = BackendModel::getURL(404);

        // parse additional variables
        if ($url404 != $url) {
            $this->tpl->assign('detailURL', SITE_URL . $url);
        }
    }

    /**
     * Validate the form
     */
    private function validateForm()
    {
        if ($this->frm->isSubmitted()) {
            $this->frm->cleanupFields();

            // validate fields
            $this->frm->getField('localgoverment')->isFilled(BL::err('LocalgovermentIsRequired'));
            $this->frm->getField('city')->isFilled(BL::err('CityIsRequired'));
            $this->frm->getField('region')->isFilled(BL::err('RegionIsRequired'));
            $this->frm->getField('street')->isFilled(BL::err('StreetIsRequired'));
            $this->frm->getField('rooms')->isFilled(BL::err('RoomsIsRequired'));
            $this->frm->getField('floor')->isFilled(BL::err('FlorIsRequired'));
            $this->frm->getField('type')->isFilled(BL::err('TypeIsRequired'));
            $this->frm->getField('material')->isFilled(BL::err('MaterialIsRequired'));
            $this->frm->getField('condition')->isFilled(BL::err('ConditionIsRequired'));
            $this->frm->getField('heating')->isFilled(BL::err('HeatingIsRequired'));
            $this->frm->getField('price')->isFilled(BL::err('PriceIsRequired'));
            $this->frm->getField('description')->isFilled(BL::err('DescriptionIsRequired'));

            if ($this->frm->isCorrect()) {
                // build item
                $item['localgoverment'] = $this->frm->getField('localgoverment')->getValue();
                $item['city'] = $this->frm->getField('city')->getValue();
                $item['region'] = $this->frm->getField('region')->getValue();
                $item['street'] = $this->frm->getField('street')->getValue();
                $item['rooms'] = $this->frm->getField('rooms')->getValue();
                $item['floor'] = $this->frm->getField('floor')->getValue();
                $item['type'] = $this->frm->getField('type')->getValue();
                $item['material'] = $this->frm->getField('material')->getValue();
                $item['condition'] = $this->frm->getField('condition')->getValue();
                $item['heating'] = $this->frm->getField('heating')->getValue();
                $item['price'] = $this->frm->getField('price')->getValue();
                $item['description'] = $this->frm->getField('description')->getValue(true);

                // save the data
                $item['id'] = BackendObjectsModel::insert($item);

                $this->redirect(
                    BackendModel::createURLForAction('Index') . '&report=added&var=' .
                    urlencode($item['street']) . '&highlight=' . $item['id']
                );
            }
        }
    }
}
