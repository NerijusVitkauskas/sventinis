<?php

namespace Backend\Modules\Objects\Actions;

use Backend\Core\Engine\Base\ActionIndex as BackendBaseActionIndex;
use Backend\Core\Engine\Authentication as BackendAuthentication;
use Backend\Core\Engine\DataGridDB as BackendDataGridDB;
use Backend\Core\Engine\DataGridArray as BackendDataGridArray;
use Backend\Core\Engine\DataGridFunctions as BackendDataGridFunctions;
use Backend\Core\Engine\Language as BL;
use Backend\Core\Engine\Model as BackendModel;
use Backend\Modules\Objects\Engine\Model as BackendObjectsModel;

class Index extends BackendBaseActionIndex
{
    public $dataGrid;

    public function execute()
    {
        parent::execute();

        $this->loadDatagrids();
        $this->parse();
        $this->display();
    }

    private function loadDatagrids()
    {
        $this->dataGrid = new BackendDataGridDB(
            BackendObjectsModel::QRY_DATAGRID_OBJECTS);

        $this->dataGrid->setAttributes(array('class' => 'dataGrid'));
        $this->dataGrid->setColumnsHidden(array('description', 'groundSpace', 'num_floors', 'year_build', 'building_status', 'area_kitchen'));
        $this->dataGrid->setRowAttributes(array('id' => '[id]'));

        // check if this action is allowed
        if (BackendAuthentication::isAllowedAction('Edit')) {
            $this->dataGrid->setColumnURL(
                'street',
                BackendModel::createURLForAction('Edit') . '&amp;id=[id]'
            );
            $this->dataGrid->addColumn(
                'edit',
                null,
                BL::lbl('Edit'),
                BackendModel::createURLForAction('Edit') . '&amp;id=[id]',
                BL::lbl('Edit')
            );
            $this->dataGrid->setColumnsHidden(array('features', 'crmId', 'expiredDate', 'land_purpose'));
        }

    }

    protected function parse()
    {
        parent::parse();

        // parse dataGrids
        if (!empty($this->dataGrid)) {
            $this->tpl->assign('dataGrid', (string)$this->dataGrid->getContent());
        }
    }
}
