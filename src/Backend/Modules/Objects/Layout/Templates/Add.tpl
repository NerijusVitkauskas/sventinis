{include:{$BACKEND_CORE_PATH}/Layout/Templates/Head.tpl}
{include:{$BACKEND_CORE_PATH}/Layout/Templates/StructureStartModule.tpl}

<div class="pageTitle">
    <h2>{$lblObjects|ucfirst}: {$lblAdd}</h2>
</div>

{form:add}
    <div class="box">
        <div class="heading">
            <h3>
                <label for="information">{$lblInformation|ucfirst}<abbr title="{$lblRequiredField}">*</abbr></label>
            </h3>
        </div>
        <label for="localgoverment">{$lblLocalgoverment|ucfirst}</label>
        {$txtLocalgoverment} {$txtLocalgovermentError}

        <label for="city">{$lblCity|ucfirst}</label>
        {$txtCity} {$txtCityError}

        <label for="region">{$lblRegion|ucfirst}</label>
        {$txtRegion} {$txtRegionError}

        <label for="street">{$lblStreet|ucfirst}</label>
        {$txtStreet} {$txtStreetError}

        <label for="rooms">{$lblRooms|ucfirst}</label>
        {$txtRooms} {$txtRoomsError}

        <label for="floor">{$lblFloor|ucfirst}</label>
        {$txtFloor} {$txtFloorError}

        <label for="type">{$lblType|ucfirst}</label>
        {$txtType} {$txtTypeError}

        <label for="material">{$lblMaterial|ucfirst}</label>
        {$txtMaterial} {$txtMaterialError}

        <label for="condition">{$lblCondition|ucfirst}</label>
        {$txtCondition} {$txtConditionError}

        <label for="heating">{$lblHeating|ucfirst}</label>
        {$txtHeating} {$txtHeatingError}

        <label for="price">{$lblPrice|ucfirst}</label>
        {$txtPrice} {$txtPriceError}
    </div>

    <div class="box">
        <div class="heading">
            <h3>
                <label for="description">{$lblDescription|ucfirst}<abbr title="{$lblRequiredField}">*</abbr></label>
            </h3>
        </div>
        <div class="optionsRTE">
            {$txtDescription} {$txtDescriptionError}
        </div>
    </div>
    <div class="fullwidthOptions">
        <div class="buttonHolderRight">
            <input id="addButton" class="inputButton button mainButton" type="submit" name="add"
                   value="{$lblAdd|ucfirst}"/>
        </div>
    </div>
{/form:add}

{include:{$BACKEND_CORE_PATH}/Layout/Templates/StructureEndModule.tpl}
{include:{$BACKEND_CORE_PATH}/Layout/Templates/Footer.tpl}
