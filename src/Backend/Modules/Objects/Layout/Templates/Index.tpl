{include:{$BACKEND_CORE_PATH}/Layout/Templates/Head.tpl}
{include:{$BACKEND_CORE_PATH}/Layout/Templates/StructureStartModule.tpl}

<div class="pageTitle">
    <h2>{$lblObjects|ucfirst}</h2>

    <div class="buttonHolderRight">
        <a href="{$var|geturl:'add'}" class="button icon iconAdd" title="{$lblAdd|ucfirst}">
            <span>{$lblAdd|ucfirst}</span>
        </a>
    </div>

    <div id="dataGridQuestionsHolder">
        {option:dataGrid}
            <br>
            <div class="dataGridHolder">
                {$dataGrid}
            </div>
        {/option:dataGrid}

        {option:!dataGrid}
        {$lblEmptyDatagrid}
        {/option:!dataGrid}
    </div>
</div>

{include:{$BACKEND_CORE_PATH}/Layout/Templates/StructureEndModule.tpl}
{include:{$BACKEND_CORE_PATH}/Layout/Templates/Footer.tpl}
