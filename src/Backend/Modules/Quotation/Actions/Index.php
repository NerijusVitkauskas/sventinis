<?php

namespace Backend\Modules\Quotation\Actions;

use Backend\Core\Engine\Base\ActionIndex as BackendBaseActionIndex;
use Backend\Core\Engine\Authentication as BackendAuthentication;
use Backend\Core\Engine\DataGridDB as BackendDataGridDB;
use Backend\Core\Engine\DataGridArray as BackendDataGridArray;
use Backend\Core\Engine\DataGridFunctions as BackendDataGridFunctions;
use Backend\Core\Engine\Language as BL;
use Backend\Core\Engine\Model as BackendModel;
use Backend\Modules\Quotation\Engine\Model as BackendQuotationModel;

class Index extends BackendBaseActionIndex
{
    public $dataGrid;

    public function execute()
    {
        parent::execute();

        $this->loadDatagrids();
        $this->parse();
        $this->display();
    }

    private function loadDatagrids()
    {
        $this->dataGrid = new BackendDataGridDB(
            BackendQuotationModel::QRY_DATAGRID_QUOTATION);

        $this->dataGrid->setAttributes(array('class' => 'dataGrid'));
        $this->dataGrid->setRowAttributes(array('id' => '[id]'));

        // check if this action is allowed
        if (BackendAuthentication::isAllowedAction('Edit')) {
            $this->dataGrid->setColumnURL(
                'quotation',
                BackendModel::createURLForAction('Edit') . '&amp;id=[id]'
            );
            $this->dataGrid->addColumn(
                'edit',
                null,
                BL::lbl('Edit'),
                BackendModel::createURLForAction('Edit') . '&amp;id=[id]',
                BL::lbl('Edit')
            );
        }

    }

    protected function parse()
    {
        parent::parse();

        // parse dataGrids
        if (!empty($this->dataGrid)) {
            $this->tpl->assign('dataGrid', (string)$this->dataGrid->getContent());
        }
    }
}
