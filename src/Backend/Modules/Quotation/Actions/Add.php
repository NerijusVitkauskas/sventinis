<?php

namespace Backend\Modules\Quotation\Actions;

use Backend\Core\Engine\Base\ActionAdd as BackendBaseActionAdd;
use Backend\Core\Engine\Authentication as BackendAuthentication;
use Backend\Core\Engine\Form as BackendForm;
use Backend\Core\Engine\Language as BL;
use Backend\Core\Engine\Model as BackendModel;
use Backend\Modules\Quotation\Engine\Model as BackendQuotationModel;


class Add extends BackendBaseActionAdd
{
    /**
     * Execute the action
     */
    public function execute()
    {
        parent::execute();

        $this->loadForm();
        $this->validateForm();

        $this->parse();
        $this->display();
    }

    /**
     * Load the form
     */
    private function loadForm()
    {
        // create form
        $this->frm = new BackendForm('add');

        $this->frm->addText('quotation', null, null, 'inputText title', 'inputTextError title');
        $this->frm->addText('author', null, null, 'inputText title', 'inputTextError title');
    }

    /**
     * Parse the page
     */
    protected function parse()
    {
        parent::parse();

        // get url
        $url = BackendModel::getURLForBlock($this->URL->getModule(), 'detail');
        $url404 = BackendModel::getURL(404);

        // parse additional variables
        if ($url404 != $url) {
            $this->tpl->assign('detailURL', SITE_URL . $url);
        }
    }

    /**
     * Validate the form
     */
    private function validateForm()
    {
        if ($this->frm->isSubmitted()) {
            $this->frm->cleanupFields();

            // validate fields
            $this->frm->getField('quotation')->isFilled(BL::err('QuotationIsRequired'));
            $this->frm->getField('author')->isFilled(BL::err('AuthorIsRequired'));

            if ($this->frm->isCorrect()) {
                // build item
                $item['quotation'] = $this->frm->getField('quotation')->getValue();
                $item['author'] = $this->frm->getField('author')->getValue();

                // save the data
                $item['id'] = BackendQuotationModel::insert($item);

                $this->redirect(
                    BackendModel::createURLForAction('Index') . '&report=added&var=' .
                    urlencode($item['author']) . '&highlight=' . $item['id']
                );
            }
        }
    }
}
