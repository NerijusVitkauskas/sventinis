<?php

namespace Backend\Modules\Quotation\Actions;

use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\HttpFoundation\File\File;

use Backend\Core\Engine\Base\ActionEdit as BackendBaseActionEdit;
use Backend\Core\Engine\Authentication as BackendAuthentication;
use Backend\Core\Engine\Model as BackendModel;
use Backend\Core\Engine\Form as BackendForm;
use Backend\Core\Engine\Language as BL;
use Backend\Modules\Quotation\Engine\Model as BackendQuotationModel;

class Edit extends BackendBaseActionEdit
{
    public $record;

    public function execute()
    {
        // get parameters
        $this->id = $this->getParameter('id', 'int');

        // does the item exists
        if ($this->id !== null && BackendQuotationModel::exists($this->id)) {
            parent::execute();

            $this->loadData();
            $this->loadForm();
            $this->validateForm();
            $this->parse();
            $this->display();
        } else {
            $this->redirect(BackendModel::createURLForAction('Index') . '&error=non-existing');
        }
    }

    private function loadData() {
        $this->record = (array) BackendQuotationModel::get($this->id);
    }
    /**
     * Load the form
     */
    private function loadForm()
    {
        // create form
        $this->frm = new BackendForm('edit');

        // create elements
        $this->frm->addText('quotation', $this->record['quotation'], null, 'inputText title', 'inputTextError title');
        $this->frm->addText('author', $this->record['author'], null, 'inputText title', 'inputTextError title');
    }

    /**
     * Parse the form
     */
    protected function parse()
    {
        parent::parse();

        // get url
        $url = BackendModel::getURLForBlock($this->URL->getModule(), 'detail');
        $url404 = BackendModel::getURL(404);

        // parse additional variables
        if ($url404 != $url) {
            $this->tpl->assign('detailURL', SITE_URL . $url);
        }

        // assign the active record and additional variables
        $this->tpl->assign('item', $this->record);
    }

    /**
     * Validate the form
     */
    private function validateForm()
    {
        // is the form submitted?
        if ($this->frm->isSubmitted()) {
            // cleanup the submitted fields, ignore fields that were added by hackers
            $this->frm->cleanupFields();

            // validate fields
            $this->frm->getField('quotation')->isFilled(BL::err('QuotationIsRequired'));
            $this->frm->getField('author')->isFilled(BL::err('AuthorIsRequired'));

            // no errors?
            if ($this->frm->isCorrect()) {
                // build item
                $item['id'] = $this->id;

                $item['quotation'] = $this->frm->getField('quotation')->getValue();
                $item['author'] = $this->frm->getField('author')->getValue();

                BackendQuotationModel::update($item);

                $redirectUrl = BackendModel::createURLForAction('Edit') .
                    '&report=saved&var=' . urlencode($item['author']) .
                    '&id=' . $item['id'] . '&draft=' . $item['id'] .
                    '&highlight=row-' . $item['id'];

                // everything is saved, so redirect to the overview
                $this->redirect($redirectUrl);
            }
        }
    }
}
