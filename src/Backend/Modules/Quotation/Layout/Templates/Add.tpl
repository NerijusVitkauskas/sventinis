{include:{$BACKEND_CORE_PATH}/Layout/Templates/Head.tpl}
{include:{$BACKEND_CORE_PATH}/Layout/Templates/StructureStartModule.tpl}

<div class="pageTitle">
    <h2>{$lblQuotation|ucfirst}</h2>
</div>

{form:add}
    <div class="box">
        <label for="name">{$lblQuotation|ucfirst}</label>
        {$txtQuotation} {$txtQuotationError}

        <label for="name">{$lblAuthor|ucfirst}</label>
        {$txtAuthor} {$txtAuthorError}
    </div>
    <div class="fullwidthOptions">
        <div class="buttonHolderRight">
            <input id="addButton" class="inputButton button mainButton" type="submit" name="add"
                   value="{$lblAdd|ucfirst}"/>
        </div>
    </div>
{/form:add}

{include:{$BACKEND_CORE_PATH}/Layout/Templates/StructureEndModule.tpl}
{include:{$BACKEND_CORE_PATH}/Layout/Templates/Footer.tpl}
