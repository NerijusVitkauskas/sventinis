<?php

namespace Backend\Modules\Quotation\Engine;

use Symfony\Component\Filesystem\Filesystem;
use Backend\Core\Engine\Authentication as BackendAuthentication;
use Backend\Core\Engine\Language as BL;
use Backend\Core\Engine\Model as BackendModel;

class Model
{
    const QRY_DATAGRID_QUOTATION =
        'SELECT *
         FROM quotation AS b
         ORDER BY b.id ASC';

    public static function insert(array $item)
    {
        $insertId = BackendModel::getContainer()->get('database')->insert('quotation', $item);
        BackendModel::invalidateFrontendCache('Quotation', BL::getWorkingLanguage());

        return $insertId;
    }

    public static function exists($id)
    {
        return (bool) BackendModel::getContainer()->get('database')->getVar(
            'SELECT b.id
             FROM quotation AS b
             WHERE b.id = ?',
            array((int) $id)
        );
    }

    public static function get($id)
    {
        return (array) BackendModel::getContainer()->get('database')->getRecord(
            'SELECT b.*
             FROM quotation AS b
             WHERE b.id = ?',
            array((int) $id)
        );
    }

    public static function update(array $item)
    {
        $updateQuotation = BackendModel::getContainer()->get('database')->update(
            'quotation',
            $item,
            'id = ?',
            array((int) $item['id'])
        );
        BackendModel::invalidateFrontendCache('Quotation', BL::getWorkingLanguage());

        return $updateQuotation;
    }

    public static function getQuotations() {
        $db = BackendModel::getContainer()->get('database');

        return (array) $db->getRecords(
            'SELECT r.*
             FROM quotation AS r
             ORDER BY id DESC');
    }
}
