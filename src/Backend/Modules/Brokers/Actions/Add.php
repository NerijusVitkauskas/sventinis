<?php

namespace Backend\Modules\Brokers\Actions;

use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\HttpFoundation\File\File;

use Backend\Core\Engine\Base\ActionAdd as BackendBaseActionAdd;
use Backend\Core\Engine\Authentication as BackendAuthentication;
use Backend\Core\Engine\Form as BackendForm;
use Backend\Core\Engine\Language as BL;
use Backend\Core\Engine\Model as BackendModel;
use Backend\Modules\Brokers\Engine\Model as BackendBrokersModel;


class Add extends BackendBaseActionAdd
{
    /**
     * Execute the action
     */
    public function execute()
    {
        parent::execute();

        $this->loadForm();
        $this->validateForm();

        $this->parse();
        $this->display();
    }

    /**
     * Load the form
     */
    private function loadForm()
    {
        // create form
        $this->frm = new BackendForm('add');

        $this->frm->addText('name', null, null, 'inputText title', 'inputTextError title');
        $this->frm->addText('phone', null, null, 'inputText title', 'inputTextError title');
        $this->frm->addText('email', null, null, 'inputText title', 'inputTextError title');
        $this->frm->addText('crmId', null, null, 'inputText title', 'inputTextError title');
        $this->frm->addCheckbox('top1');
        $this->frm->addCheckbox('top2');
        $this->frm->addCheckbox('top3');
        $this->frm->addText('top');
        $this->frm->addEditor('description');
        $this->frm->addImage('image');
    }

    /**
     * Parse the page
     */
    protected function parse()
    {
        parent::parse();

        // get url
        $url = BackendModel::getURLForBlock($this->URL->getModule(), 'detail');
        $url404 = BackendModel::getURL(404);

        // parse additional variables
        if ($url404 != $url) {
            $this->tpl->assign('detailURL', SITE_URL . $url);
        }
    }

    /**
     * Validate the form
     */
    private function validateForm()
    {
        if ($this->frm->isSubmitted()) {
            $this->frm->cleanupFields();

            // validate fields
            $this->frm->getField('name')->isFilled(BL::err('NameIsRequired'));
            $this->frm->getField('phone')->isFilled(BL::err('PhoneIsRequired'));
            $this->frm->getField('email')->isFilled(BL::err('EmailIsRequired'));
            $this->frm->getField('description')->isFilled(BL::err('DescriptionIsRequired'));

            if ($this->frm->getField('image')->isFilled()) {
                $this->frm->getField('image')->isAllowedExtension(array('jpg', 'png', 'gif', 'jpeg'), BL::err('JPGGIFAndPNGOnly'));
                $this->frm->getField('image')->isAllowedMimeType(array('image/jpg', 'image/png', 'image/gif', 'image/jpeg'), BL::err('JPGGIFAndPNGOnly'));
            }

            if ($this->frm->isCorrect()) {
                // build item
                $item['name'] = $this->frm->getField('name')->getValue();
                $item['phone'] = $this->frm->getField('phone')->getValue();
                $item['email'] = $this->frm->getField('email')->getValue();
                $item['crmId'] = $this->frm->getField('crmId')->getValue();
                $item['description'] = $this->frm->getField('description')->getValue(true);
                $item['level'] = $this->frm->getField('top')->getValue();

                $imagePath = FRONTEND_FILES_PATH . '/brokers';

                // create folders if needed
                $fs = new Filesystem();
                if (!$fs->exists($imagePath . '/source')) {
                    $fs->mkdir($imagePath . '/source');
                }
                if (!$fs->exists($imagePath . '/220x290')) {
                    $fs->mkdir($imagePath . '/220x290');
                }
                if (!$fs->exists($imagePath . '/128x128')) {
                    $fs->mkdir($imagePath . '/128x128');
                }
                if (!$fs->exists($imagePath . '/x64')) {
                    $fs->mkdir($imagePath . '/x64');
                }

                if ($this->frm->getField('image')->isFilled()) {
                    $image = $this->frm->getField('image')->getFileName();
                    if($fs->exists($imagePath.'/source/'.$image))
                    {
                        $i = 1;
                        while($fs->exists($imagePath.'/source/'.$this->frm->getField('image')->getFileName(false).'('.$i.')' . '.' . $this->frm->getField('image')->getExtension()))
                        {
                            $i++;
                        }
                        $image = $this->frm->getField('image')->getFileName(false).'exist'.$i.'exist' . '.' . $this->frm->getField('image')->getExtension();
                    }

                    // upload the image & generate thumbnails
                    $this->frm->getField('image')->generateThumbnails($imagePath, $image, true);
                }
                $item['id'] = BackendBrokersModel::insert($item);

                if(isset($image)) {
                    BackendBrokersModel::insertPhoto(array('broker' => $item['id'], 'photo' => $image));
                }

                // save the data

                $this->redirect(
                    BackendModel::createURLForAction('Index') . '&report=added&var=' .
                    urlencode($item['name']) . '&highlight=' . $item['id']
                );
            }
        }
    }
}
