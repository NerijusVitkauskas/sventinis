<?php

namespace Backend\Modules\Brokers\Actions;

use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\HttpFoundation\File\File;

use Backend\Core\Engine\Base\ActionEdit as BackendBaseActionEdit;
use Backend\Core\Engine\Authentication as BackendAuthentication;
use Backend\Core\Engine\Model as BackendModel;
use Backend\Core\Engine\Form as BackendForm;
use Backend\Core\Engine\Language as BL;
use Backend\Modules\Brokers\Engine\Model as BackendBrokersModel;

class Edit extends BackendBaseActionEdit
{
    public $record;
    public $feedBacks;

    public function execute()
    {
        // get parameters
        $this->id = $this->getParameter('id', 'int');

        // does the item exists
        if ($this->id !== null && BackendBrokersModel::exists($this->id)) {
            parent::execute();

            $this->loadData();
            $this->loadForm();
            $this->validateForm();
            $this->parse();
            $this->display();
        } else {
            $this->redirect(BackendModel::createURLForAction('Index') . '&error=non-existing');
        }
    }

    private function loadData() {
        $this->record = (array) BackendBrokersModel::get($this->id);
        $this->feedBacks = (array) BackendBrokersModel::getFeedbacks($this->id);
    }
    /**
     * Load the form
     */
    private function loadForm()
    {
        // create form
        $this->frm = new BackendForm('edit');

        // create elements
        $this->frm->addText('name', $this->record['name'], null, 'inputText title', 'inputTextError title');
        $this->frm->addText('phone', $this->record['phone'], null, 'inputText title', 'inputTextError title');
        $this->frm->addText('email', $this->record['email'], null, 'inputText title', 'inputTextError title');
        $this->frm->addText('crmId', $this->record['crmId'], null, 'inputText title', 'inputTextError title');
        $this->frm->addCheckbox('top1');
        $this->frm->addCheckbox('top2');
        $this->frm->addCheckbox('top3');
        $this->frm->addText('top');
        $this->frm->addEditor('description', $this->record['description']);
        $this->frm->addImage('image');
    }

    /**
     * Parse the form
     */
    protected function parse()
    {
        parent::parse();

        // get url
        $url = BackendModel::getURLForBlock($this->URL->getModule(), 'detail');
        $url404 = BackendModel::getURL(404);

        // parse additional variables
        if ($url404 != $url) {
            $this->tpl->assign('detailURL', SITE_URL . $url);
        }

        // assign the active record and additional variables
        $this->tpl->assign('item', $this->record);


        if($this->record['level'] == 1) {
            $this->tpl->assign('manager', 'Y');
        } elseif($this->record['level'] == 4) {
            $this->tpl->assign('top1', 'Y');
        } elseif($this->record['level'] == 3) {
            $this->tpl->assign('top2', 'Y');
        } elseif($this->record['level'] == 2) {
            $this->tpl->assign('top3', 'Y');
        } else {
            $this->tpl->assign('topNone', 'Y');
        }

        $this->tpl->assign('item', $this->record);
        $this->tpl->assign('feedBacks', $this->feedBacks);
        $this->tpl->assign('photo', BackendBrokersModel::getPhoto($this->record['id']));

    }

    /**
     * Validate the form
     */
    private function validateForm()
    {
        // is the form submitted?
        if ($this->frm->isSubmitted()) {
            // cleanup the submitted fields, ignore fields that were added by hackers
            $this->frm->cleanupFields();

            // validate fields
            $this->frm->getField('name')->isFilled(BL::err('NameIsRequired'));
            $this->frm->getField('phone')->isFilled(BL::err('PhoneIsRequired'));
            $this->frm->getField('email')->isFilled(BL::err('EmailIsInvalid'));
            $this->frm->getField('description')->isFilled(BL::err('DescriptionIsInvalid'));

            if ($this->frm->getField('image')->isFilled()) {
                $this->frm->getField('image')->isAllowedExtension(array('jpg', 'png', 'gif', 'jpeg'), BL::err('JPGGIFAndPNGOnly'));
                $this->frm->getField('image')->isAllowedMimeType(array('image/jpg', 'image/png', 'image/gif', 'image/jpeg'), BL::err('JPGGIFAndPNGOnly'));
            }

            // no errors?
            if ($this->frm->isCorrect()) {
                // build item
                $item['id'] = $this->id;

                $item['name'] = $this->frm->getField('name')->getValue();
                $item['phone'] = $this->frm->getField('phone')->getValue();
                $item['email'] = $this->frm->getField('email')->getValue();
                $item['crmId'] = $this->frm->getField('crmId')->getValue();
                $item['description'] = $this->frm->getField('description')->getValue();
                $item['level'] = $this->frm->getField('top')->getValue();

                $imagePath = FRONTEND_FILES_PATH . '/brokers';

                // create folders if needed
                $fs = new Filesystem();
                if (!$fs->exists($imagePath . '/source')) {
                    $fs->mkdir($imagePath . '/source');
                }
                if (!$fs->exists($imagePath . '/220x290')) {
                    $fs->mkdir($imagePath . '/220x290');
                }
                if (!$fs->exists($imagePath . '/128x128')) {
                    $fs->mkdir($imagePath . '/128x128');
                }
                if (!$fs->exists($imagePath . '/x64')) {
                    $fs->mkdir($imagePath . '/x64');
                }

                if ($this->frm->getField('image')->isFilled()) {
                    $image = $this->frm->getField('image')->getFileName();
                    if($fs->exists($imagePath.'/source/'.$image))
                    {
                        $i = 1;
                        while($fs->exists($imagePath.'/source/'.$this->frm->getField('image')->getFileName(false).'('.$i.')' . '.' . $this->frm->getField('image')->getExtension()))
                        {
                            $i++;
                        }
                        $image = $this->frm->getField('image')->getFileName(false).'exist'.$i.'exist' . '.' . $this->frm->getField('image')->getExtension();
                    }

                    // upload the image & generate thumbnails
                    $this->frm->getField('image')->generateThumbnails($imagePath, $image, true);
                }

                if(isset($image)) {
                    BackendBrokersModel::insertPhoto(array('broker' => $this->record['id'], 'photo' => $image));
                }

                BackendBrokersModel::update($item);

                $redirectUrl = BackendModel::createURLForAction('Edit') .
                    '&report=saved&var=' . urlencode($item['name']) .
                    '&id=' . $item['id'] . '&draft=' . $item['id'] .
                    '&highlight=row-' . $item['id'];

                // everything is saved, so redirect to the overview
                $this->redirect($redirectUrl);
            }
        }
    }
}
