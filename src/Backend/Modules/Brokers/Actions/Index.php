<?php

namespace Backend\Modules\Brokers\Actions;

use Backend\Core\Engine\Base\ActionIndex as BackendBaseActionIndex;
use Backend\Core\Engine\Authentication as BackendAuthentication;
use Backend\Core\Engine\DataGridDB as BackendDataGridDB;
use Backend\Core\Engine\DataGridArray as BackendDataGridArray;
use Backend\Core\Engine\DataGridFunctions as BackendDataGridFunctions;
use Backend\Core\Engine\Language as BL;
use Backend\Core\Engine\Model as BackendModel;
use Backend\Modules\Brokers\Engine\Model as BackendBrokersModel;

class Index extends BackendBaseActionIndex
{
    public $dataGrid;

    public function execute()
    {
        parent::execute();

        $this->loadDatagrids();
        $this->parse();
        $this->display();
    }

    private function loadDatagrids()
    {
        $this->dataGrid = new BackendDataGridDB(
            BackendBrokersModel::QRY_DATAGRID_BROKERS);

        $this->dataGrid->setAttributes(array('class' => 'dataGrid'));
        $this->dataGrid->setColumnsHidden(array('description'));
        $this->dataGrid->setRowAttributes(array('id' => '[id]'));
        $this->dataGrid->setColumnFunction(
            array(__CLASS__, 'level'),
            array('[level]'),
            'level',
            true
        );

        // check if this action is allowed
        if (BackendAuthentication::isAllowedAction('Edit')) {
            $this->dataGrid->setColumnURL(
                'name',
                BackendModel::createURLForAction('Edit') . '&amp;id=[id]'
            );
            $this->dataGrid->addColumn(
                'edit',
                null,
                BL::lbl('Edit'),
                BackendModel::createURLForAction('Edit') . '&amp;id=[id]',
                BL::lbl('Edit')
            );
        }

    }

    public static function level($var){
        if ($var == 0) {
            return "Jokio";
        }
        if ($var == 1) {
            return "Vadovas";
        }
        if ($var == 4) {
            return "Top 1";
        }
        if ($var == 3) {
            return "Top 2";
        }
        if ($var == 2) {
            return "Top 3";
        }
    }

    protected function parse()
    {
        parent::parse();

        // parse dataGrids
        if (!empty($this->dataGrid)) {
            $this->tpl->assign('dataGrid', (string)$this->dataGrid->getContent());
        }
    }
}
