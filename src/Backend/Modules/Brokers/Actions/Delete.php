<?php

namespace Backend\Modules\Brokers\Actions;

use Backend\Core\Engine\Base\ActionAdd as BackendBaseActionAdd;
use Backend\Core\Engine\Language as BL;
use Backend\Core\Engine\Model as BackendModel;
use Backend\Modules\Brokers\Engine\Model as BackendBrokersModel;


class Delete extends BackendBaseActionAdd
{
    public function execute()
    {
        $this->id = $this->getParameter('id', 'int');

        if ($this->id !== null) {
            parent::execute();

            BackendBrokersModel::delete('brokers', $this->id);

            $redirectUrl = BackendModel::createURLForAction('Index') . '&report=deleted';
            $this->redirect($redirectUrl);
        } else {
            $this->redirect(BackendModel::createURLForAction('Index') . '&error=non-existing');
        }
    }
}
