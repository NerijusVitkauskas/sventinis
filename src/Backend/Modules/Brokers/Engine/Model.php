<?php

namespace Backend\Modules\Brokers\Engine;

use Symfony\Component\Filesystem\Filesystem;
use Backend\Core\Engine\Authentication as BackendAuthentication;
use Backend\Core\Engine\Language as BL;
use Backend\Core\Engine\Model as BackendModel;

class Model
{
    const QRY_DATAGRID_BROKERS =
        'SELECT *
         FROM brokers AS b
         ORDER BY b.id ASC';

    public static function insert(array $item)
    {
        $insertId = BackendModel::getContainer()->get('database')->insert('brokers', $item);
        BackendModel::invalidateFrontendCache('Brokers', BL::getWorkingLanguage());

        return $insertId;
    }

    public static function exists($id)
    {
        return (bool) BackendModel::getContainer()->get('database')->getVar(
            'SELECT b.id
             FROM brokers AS b
             WHERE b.id = ?',
            array((int) $id)
        );
    }

    public static function get($id)
    {
        return (array) BackendModel::getContainer()->get('database')->getRecord(
            'SELECT b.*
             FROM brokers AS b
             WHERE b.id = ?',
            array((int) $id)
        );
    }

    public static function getPhoto($id)
    {
        return (array) BackendModel::getContainer()->get('database')->getRecord(
            'SELECT b.*
             FROM brokers_photos AS b
             WHERE b.broker = ? ORDER BY b.id DESC',
            array((int) $id)
        );
    }

    public static function getFeedbacks($id)
    {
        return (array) BackendModel::getContainer()->get('database')->getRecords(
            'SELECT b.*
             FROM brokers_feedbacks AS b
             WHERE b.broker = ? ORDER BY b.id DESC',
            array((int) $id)
        );
    }

    public static function update(array $item)
    {
        $updateBroker = BackendModel::getContainer()->get('database')->update(
            'brokers',
            $item,
            'id = ?',
            array((int) $item['id'])
        );

        // invalidate the cache for blog
        BackendModel::invalidateFrontendCache('Brokers', BL::getWorkingLanguage());

        // return the new revision id
        return $updateBroker;
    }

    public static function insertPhoto(array $item)
    {
        $insertId = BackendModel::getContainer()->get('database')->insert('brokers_photos', $item);
        BackendModel::invalidateFrontendCache('Brokers', BL::getWorkingLanguage());

        return $insertId;
    }

    public static function getBrokers() {
        $db = BackendModel::getContainer()->get('database');

        return (array) $db->getRecords(
            'SELECT r.*
             FROM brokers AS r
             ORDER BY id DESC');
    }

    public static function delete($table, $value, $where = 'id = ?'){

        // get db
        $db = BackendModel::getContainer()->get('database');

        $db->delete($table, $where, $value);
    }
}
