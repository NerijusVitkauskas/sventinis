{include:{$BACKEND_CORE_PATH}/Layout/Templates/Head.tpl}
{include:{$BACKEND_CORE_PATH}/Layout/Templates/StructureStartModule.tpl}

<ul class="tabrow">
    <li class="selected" id="brokerInfo"><a href="">Informacija</a></li>
    <li id="brokerFeedback"><a href="">Atsiliepimai</a></li>
</ul>
<div class="pageTitle">
    <h2>{$lblBrokers|ucfirst}: {$lblEdit}</h2>
</div>

<div class="brokerInfoTab">
{form:edit}
    <div class="box">
        <div class="heading">
            <h3>
                <label for="information">{$lblInformation|ucfirst}<abbr title="{$lblRequiredField}">*</abbr></label>
            </h3>
        </div>
        <label for="name">{$lblName|ucfirst}</label>
        {$txtName} {$txtNameError}

        <label for="name">{$lblPhone|ucfirst}</label>
        {$txtPhone} {$txtPhoneError}

        <label for="name">{$lblEmail|ucfirst}</label>
        {$txtEmail} {$txtEmailError}

        <label for="name">{$lblCrmId|ucfirst}</label>
        {$txtCrmId} {$txtCrmIdError}
        <br>        <br>

        <label for="top3">{$lblManager|ucfirst}</label><input type="radio" value="1" name="top"{option:manager} checked{/option:manager}>
        <label for="top3">{$lblTop1|ucfirst}</label><input type="radio" value="4" name="top"{option:top1} checked{/option:top1}>
        <label for="top3">{$lblTop2|ucfirst}</label><input type="radio" value="3" name="top"{option:top2} checked{/option:top2}>
        <label for="top3">{$lblTop3|ucfirst}</label><input type="radio" value="2" name="top"{option:top3} checked{/option:top3}>
        <label for="topnone">{$lblTopnone|ucfirst}</label><input type="radio" value="0" name="top"{option:topNone} checked{/option:topNone}>

    </div>
    <div class="box">
        <div class="heading">
            <h3>
                <label for="description">{$lblDescription|ucfirst}<abbr title="{$lblRequiredField}">*</abbr></label>
            </h3>
        </div>
        <div class="optionsRTE">
            {$txtDescription} {$txtDescriptionError}
        </div>
    </div>
    <div class="box">
        <div class="heading">
            <h3>{$lblImage|ucfirst}</h3>
        </div>
        <div class="options clearfix">
            {option:photo.photo}
                <p class="imageHolder">
                    <img src="{$FRONTEND_FILES_URL}/brokers/220x290/{$photo.photo}" height="150" alt="{$lblImage|ucfirst}" />
                </p>
            {/option:photo.photo}
            <p>
                {$fileImage} {$fileImageError}
            </p>
        </div>
    </div>
    <div class="fullwidthOptions">
        <div class="buttonHolderRight">
            <input id="editButton" class="inputButton button mainButton" type="submit" name="edit"
                   value="{$lblEdit|ucfirst}"/>
        </div>
    </div>
    <a href="{$var|geturl:'Delete'}&amp;id={$item.id}" data-message-id="confirmDelete" class="askConfirmation button linkButton icon iconDelete">
        <span>{$lblDelete|ucfirst}</span>
    </a>

    <div id="confirmDelete" title="{$lblDelete|ucfirst}?" style="display: none;">
        <p>
            Ar tikrai norite ištrinti?
        </p>
    </div>
{/form:edit}
</div>

<div class="brokerFeedbackTab" style="display: none;">

{option:feedBacks}
{iteration:feedBacks}
    <div class="box">
        <div class="heading">
            <h3>
                <label>{$feedBacks.name} -- {$feedBacks.email}-- {$feedBacks.date}</label>
                <a class="feedbackDeleteLink" href="{$var|geturl:'DeleteFeedback'}&amp;id={$feedBacks.id}&broker={$item.id}">Trinti</a>
            </h3>
        </div>
        <div class="options clearfix">
        {$feedBacks.feedback}
        </div>
    </div>
{/iteration:feedBacks}
{/option:feedBacks}
</div>

{include:{$BACKEND_CORE_PATH}/Layout/Templates/StructureEndModule.tpl}
{include:{$BACKEND_CORE_PATH}/Layout/Templates/Footer.tpl}
