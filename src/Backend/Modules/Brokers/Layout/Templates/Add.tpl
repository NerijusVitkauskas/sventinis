{include:{$BACKEND_CORE_PATH}/Layout/Templates/Head.tpl}
{include:{$BACKEND_CORE_PATH}/Layout/Templates/StructureStartModule.tpl}

<div class="pageTitle">
    <h2>{$lblBrokers|ucfirst}: {$lblAdd}</h2>
</div>

{form:add}
    <div class="box">
        <div class="heading">
            <h3>
                <label for="information">{$lblInformation|ucfirst}<abbr title="{$lblRequiredField}">*</abbr></label>
            </h3>
        </div>
        <label for="name">{$lblName|ucfirst}</label>
        {$txtName} {$txtNameError}

        <label for="name">{$lblPhone|ucfirst}</label>
        {$txtPhone} {$txtPhoneError}

        <label for="name">{$lblEmail|ucfirst}</label>
        {$txtEmail} {$txtEmailError}

        <label for="name">{$lblCrmId|ucfirst}</label>
        {$txtCrmId} {$txtCrmIdError}
        <br><br>

        <label for="top3">{$lblManager|ucfirst}</label><input type="radio" value="1" name="top"{option:manager} checked{/option:manager}>
        <label for="top3">{$lblTop1|ucfirst}</label><input type="radio" value="2" name="top"{option:top1} checked{/option:top1}>
        <label for="top3">{$lblTop2|ucfirst}</label><input type="radio" value="3" name="top"{option:top2} checked{/option:top2}>
        <label for="top3">{$lblTop3|ucfirst}</label><input type="radio" value="4" name="top"{option:top3} checked{/option:top3}>
        <label for="topnone">{$lblTopnone|ucfirst}</label><input type="radio" value="0" name="top"{option:topNone} checked{/option:topNone}>


    </div>
    <div class="box">
        <div class="heading">
            <h3>
                <label for="description">{$lblDescription|ucfirst}<abbr title="{$lblRequiredField}">*</abbr></label>
            </h3>
        </div>
        <div class="optionsRTE">
            {$txtDescription} {$txtDescriptionError}
        </div>
    </div>
    <div class="box">
        <div class="heading">
            <h3>{$lblImage|ucfirst}</h3>
        </div>
        <div class="options clearfix">
            <p>
                {$fileImage} {$fileImageError}
            </p>
        </div>
    </div>
    <div class="fullwidthOptions">
        <div class="buttonHolderRight">
            <input id="addButton" class="inputButton button mainButton" type="submit" name="add"
                   value="{$lblAdd|ucfirst}"/>
        </div>
    </div>
{/form:add}

{include:{$BACKEND_CORE_PATH}/Layout/Templates/StructureEndModule.tpl}
{include:{$BACKEND_CORE_PATH}/Layout/Templates/Footer.tpl}
