<?php

// autoload_namespaces.php @generated by Composer

$vendorDir = dirname(dirname(__FILE__));
$baseDir = dirname($vendorDir);

return array(
    'Twig_' => array($vendorDir . '/twig/twig/lib'),
    'TijsVerkoyen\\CssToInlineStyles' => array($vendorDir . '/tijsverkoyen/css-to-inline-styles'),
    'Symfony\\Bundle\\MonologBundle' => array($vendorDir . '/symfony/monolog-bundle'),
    'Symfony\\' => array($vendorDir . '/symfony/symfony/src'),
    'Psr\\Log\\' => array($vendorDir . '/psr/log'),
    'Doctrine\\Common\\Lexer\\' => array($vendorDir . '/doctrine/lexer/lib'),
    'Doctrine\\Common\\Inflector\\' => array($vendorDir . '/doctrine/inflector/lib'),
    'Doctrine\\Common\\Collections\\' => array($vendorDir . '/doctrine/collections/lib'),
    'Doctrine\\Common\\Cache\\' => array($vendorDir . '/doctrine/cache/lib'),
    'Doctrine\\Common\\Annotations\\' => array($vendorDir . '/doctrine/annotations/lib'),
    'Doctrine\\Common\\' => array($vendorDir . '/doctrine/common/lib'),
    'Behat\\Transliterator' => array($vendorDir . '/behat/transliterator/src'),
    'Assetic' => array($vendorDir . '/kriswallsmith/assetic/src'),
    '' => array($baseDir . '/src'),
);
