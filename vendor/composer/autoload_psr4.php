<?php

// autoload_psr4.php @generated by Composer

$vendorDir = dirname(dirname(__FILE__));
$baseDir = dirname($vendorDir);

return array(
    'Symfony\\Bundle\\AsseticBundle\\' => array($vendorDir . '/symfony/assetic-bundle'),
    'Monolog\\' => array($vendorDir . '/monolog/monolog/src/Monolog'),
    'MatthiasMullie\\Minify\\' => array($vendorDir . '/matthiasmullie/minify/src'),
);
